#RopMount - a Roping Assistant

##What is RopMount

The goal of RopMount is to help ROP by finding gadgets.
RopMount is based on instructions breaking: it will search gadget
on every executable segment and will search them 'between' legit
instructions of the program.
RopMount is also able to assemble some RopStack for you, based on
what you need and the gadgets found in the executable.

For now RopMount can work on Elf32, PE and Elf64 files.

##Using RopMount
### Installation
    python setup.py develop

###Launch it
    ropmount --dump template [template [...]] progname
    or
    ropmount --ass progname


##Dumper overview
RopMount gadgets search is based on filters.
So RopMount will output all gadgets that match at least
one filter you give to it.
Filters are described using a kind of template asm:

        mov esp, [REG32 + CONST]; {0,3}ROP; RET

 - REG32 matches any 32bits register,
 - REG64 matches any 64bits register (for Elf64),
 - CONST matches any immediate,
 - ROP matches any ROP-valid instruction,
 - {0,3}pop REG matches 0 to 3 'ROP',
 - RET is an alias for 'ret' or 'retn CONST'.

So the previous template could match:

    - mov esp, [EAX + 0x2]; pop EAX; POP EDX; ret
    - mov esp, [EDX + 0x42]; inc edx; add eax, 0x10; retn 0xC

###Template syntax
Complete template syntax is available in `doc/template/syntax.txt`.

###Predef template
Some template as 'stackpivot' are already built-in.
Complete list is available in `doc/template/predef.txt`.

##Assembler overview
RopMount can assemble RopStack for you!
For example:

    >>> mov eax, esp

This command asks a RopStack to mov esp to eax.  
RopMount will try using different methods:

 - finding simple mov,
 - finding 'push REG32 ; pop REG32 gadgets',
 - finding mov based on arithmetic (add / xor / ...),
 - chaining these movs to find a result.

So the previous command could be assembled as:

    mov edx, esp; ret
    push edx; pop eax; ret

Remember that RopMount breaks instructions so it can find gadgets that are not
part of the real instruction flow.

### Assembler instruction set and example

Find complete RopAsm presentation at:  
    - RopAsm presentation : `RopAsm.md`  
Also:  
    - RopAsm X86 instruction set quickref : `doc/ropasm/x86quickref.txt`  
    - Complete RopAsm X86 instruction set " `doc/ropasm/x86instrset.txt`  
    - Complete RopAsm X86_64 instruction set " `doc/ropasm/x86_64instrset.txt`  

##RopMount as a module!

RopMount can now be imported as a python module.

## Find gadgets / Assemble stacks in your own scripts

    :::python
    import ropmount
    rpc = ropmount.rop_from_files(["./bin", "./libc.so"])
    pop_pop_ret = rpc.find("{2} pop REG32; ret")
    ropchain = rpc.assemble("call exit,42")

### Dump your ropchain in any format you want.

    :::python
    ropchain.stack.dump('raw')
    ropchain.stack.dump('python')
    my_own_dumper(ropchain.stack.dump())

### Extract symbols, leak and bypass ASLR easily
    :::python
    #This is how you can get the GOT entry to leak
    rpc.get_symbols()['read.got']

    #Assemble a stack that will leak the read GOT entry on fd `FD`
    rpc.assemble("leak read.got,FD")

    # Set libc base addr based on read addr !
    rpc2.mapfile[LIBC_FILENAME].fix_base_from_symbol('read', leaked_addr)

    #ropmount is now ready to assemble stack using gadget in the libc !

Complete example in `doc/example/module.py`

##Get RopMount

### Latest version
Latest version can be found at:  
https://bitbucket.org/Hakril/ropmount

### Requirements
RopMount is based on BeaEngine (http://www.beaengine.org/).  
So you will need:

* python2 (>= 2.6),
* BeaEngine lib: http://www.beaengine.org/ ,
* BeaEnginePython: (http://pypi.python.org/pypi/BeaEnginePython/).


##Improve RopMount
If you use RopMount, any kind of feedback would be useful!  
You can use the issue tracker or email me at <hakril@lse.epita.fr>

##Contact

 * Mail: <hakril@lse.epita.fr>
 * Twitter: @hakril
