#! /usr/bin/python2

import ropmount
from ropmount.archi.type import RelocableImmediat

def Entry(filenames, templates):
    """entry point of the dumper front-end"""
    rpc = ropmount.rop_from_files(filenames)
    predef_template = rpc.archi.predef_template
    names = []

    if 'all' in templates:
        templates.remove('all')
        templates += predef_template

    raw_templates = []
    for template in templates:
        if template.strip() in predef_template:
            raw_templates.extend([temp for temp in predef_template[template.strip()]])
        else:
            raw_templates.append(template)

    x = rpc.find_list(raw_templates)
    for name, dg in zip(raw_templates, x):
        print '---'
        print name
        if not len(dg):
            print "[] None"
            continue
        for ropitem  in dg:
            if isinstance(ropitem.vaddr, RelocableImmediat):
                descr = "[base + {0}] {1}".format(ropitem.vaddr, ropitem.descr)
            else:
                descr = "[{0}] {1}".format(ropitem.vaddr, ropitem.descr)
            print descr
