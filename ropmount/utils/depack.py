#! /usr/bin/python2
"""
    A module to extract struct from a file
"""
import struct

#Constants
BYTE = "B"
WORD = "H"
DWORD = "I"
LONG = "L"

BIG_ENDIAN = ">"
LITTLE_ENDIAN = "<"


class DescriptionError(Exception):
    pass

def padding(fmt):
    """Add padding in struct description"""
    return "x" * (struct.calcsize(fmt) - 1) + "B"

def _extract_str(str, file, endianness):
    struct_str = endianness + str
    unpack = struct.unpack(struct_str, file.read(struct.calcsize(struct_str)))
    if len(unpack) == 1:
        i, = unpack
        return i
    return list(unpack)

def depack(descr, file, endiannes = "<"):
    """
        Extract the struct:
        Return a dict of form {'field name': value}
    """
    res_struct = dict()
    for field, value in iter(descr):
        if isinstance(value, basestring):
            res_struct.update({field : _extract_str(value, file, endiannes)})
            continue
        if isinstance(value, list):
            res_struct.update({field : depack(value, file, endiannes)})
            continue
        if isinstance(value, tuple):
            res = []
            for descr in value:
                res.append(depack(descr, file, endiannes))
            res_struct.update({field : res})
            continue
        raise DescriptionError("Unhandled type for field : " + field)
    return res_struct
