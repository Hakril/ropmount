
class Recoverable(type):
    """
        This type describe class that instance may be recovered without using __new__/__init__
        If cls._recover return not None : this will be returned as new instance of cls
    """
    def __call__(cls, *args, **kargs):
        try:
            if not cls.recover:
                return super(Recoverable, Recoverable).__call__(cls, *args, **kargs)
        except AttributeError:
            pass

        x = cls._recover(*args, **kargs)
        if x is not None:
            return x
        return super(Recoverable, Recoverable).__call__(cls, *args, **kargs)

