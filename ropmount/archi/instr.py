#!/usr/bin/python2

class InstrDisas(object):
    def stack_consumption(self):
        """Return the number of byte of stack comsumed by the instruction"""
        raise NotImplementedError()

    def is_valid_rop(self):
        """Return True if Instr is okay to appear in a ROP"""
        raise NotImplementedError()

    def is_end_rop(self):
        """
            Return true if the Instr can't stand in the middle of ANY gadget.
            Any Instruction that is 'end_rop' can only be found at the end of the gadget.
            Example : x86 'ret' is a typical 'end_rop' instruction.
        """
        raise NotImplementedError()

    def invalid_register(self):
        """Return a RegSet of invalidated Register"""
        raise NotImplementedError()
