from ..intelregset import IntelRegSet
from .x86_64type import X86_64Register

class X86_64RegSet(IntelRegSet):
    register_type = X86_64Register
    #BEURK !
    register_dep = {
         #Basic X86
         X86_64Register("EAX") : map(X86_64Register,["AX"]),
         X86_64Register("AX") : map(X86_64Register,["AH","AL"]),
         X86_64Register("EBX") : map(X86_64Register,["BX"]),
         X86_64Register("BX") : map(X86_64Register,["BH","BL"]),
         X86_64Register("ECX") : map(X86_64Register,["CX"]),
         X86_64Register("CX") : map(X86_64Register,["CH","CL"]),
         X86_64Register("EDX") : map(X86_64Register,["DX"]),
         X86_64Register("DX") : map(X86_64Register,["DH","DL"]),
         X86_64Register("EBP") : map(X86_64Register,["BP"]),
         X86_64Register("ESP") : map(X86_64Register,["SP"]),
         X86_64Register("EDI") : map(X86_64Register,["DI"]),
         X86_64Register("ESI") : map(X86_64Register,["SI"]),

         X86_64Register("BP") : map(X86_64Register,["BPL"]),
         X86_64Register("SP") : map(X86_64Register,["SPL"]),
         X86_64Register("DI") : map(X86_64Register,["DIL"]),
         X86_64Register("SI") : map(X86_64Register,["SIL"]),

         X86_64Register("RAX") : map(X86_64Register,["EAX"]),
         X86_64Register("RBX") : map(X86_64Register,["EBX"]),
         X86_64Register("RCX") : map(X86_64Register,["ECX"]),
         X86_64Register("RDX") : map(X86_64Register,["EDX"]),
         X86_64Register("RBP") : map(X86_64Register,["EBP"]),
         X86_64Register("RSP") : map(X86_64Register,["ESP"]),

         X86_64Register("R8") : [X86_64Register("R8D")],
         X86_64Register("R9") : [X86_64Register("R9D")],
         X86_64Register("R10"): [X86_64Register("R10D")],
         X86_64Register("R11"): [X86_64Register("R11D")],
         X86_64Register("R12"): [X86_64Register("R12D")],
         X86_64Register("R13"): [X86_64Register("R13D")],
         X86_64Register("R14"): [X86_64Register("R14D")],
         X86_64Register("R15"): [X86_64Register("R15D")],

         X86_64Register("R8D") : [X86_64Register("R8W")],
         X86_64Register("R9D") : [X86_64Register("R9W")],
         X86_64Register("R10D"): [X86_64Register("R10W")],
         X86_64Register("R11D"): [X86_64Register("R11W")],
         X86_64Register("R12D"): [X86_64Register("R12W")],
         X86_64Register("R13D"): [X86_64Register("R13W")],
         X86_64Register("R14D"): [X86_64Register("R14W")],
         X86_64Register("R15D"): [X86_64Register("R15W")],

         X86_64Register("R8W") : [X86_64Register("R8L")],
         X86_64Register("R9W") : [X86_64Register("R9L")],
         X86_64Register("R10W"): [X86_64Register("R10L")],
         X86_64Register("R11W"): [X86_64Register("R11L")],
         X86_64Register("R12W"): [X86_64Register("R12L")],
         X86_64Register("R13W"): [X86_64Register("R13L")],
         X86_64Register("R14W"): [X86_64Register("R14L")],
         X86_64Register("R15W"): [X86_64Register("R15L")],
         }
