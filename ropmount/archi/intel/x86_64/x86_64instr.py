from ..intelinstr import IntelInstrDisas
from .x86_64type import X86_64Immediat, X86_64Register, X86_64MemAccess, X86_64RelocableImmediat
from .x86_64regset import X86_64RegSet


class X86_64InstrDisas(IntelInstrDisas):
    RegSet =   X86_64RegSet
    Immediat = X86_64Immediat
    MemAccess = X86_64MemAccess
    Register = X86_64Register
    StackReg = X86_64Register("RSP")
