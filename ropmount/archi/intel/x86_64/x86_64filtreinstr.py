from ..x86.x86filtreinstr import x86_filter_instrs
from .x86_64type import X86_64Register

x86_64_filter_instrs = dict(x86_filter_instrs)


x86_64_reg_group = {
        "REG32" : map(X86_64Register, X86_64Register.valid32),
        "REG64" : map(X86_64Register, X86_64Register.valid64),
        "REG" : map(X86_64Register, X86_64Register.valid_regs)
        }

x86_64_predef_template = {
'stackpivot' : ['mov rsp, [REG64]; {,}ROP; RET',
                'mov rsp,[REG64 + CONST]; {,}ROP; RET',
                'mov rsp, REG64 ; {,}ROP; RET',
                'pop rsp; {,}ROP; RET',
                'xchg rsp, REG64; {,}ROP; RET',
                'xchg REG64, rsp; {,}ROP; RET',
                'push REG64;{,}ROP ;pop rsp; {,}ROP; RET',
                'push REG64; {,}ROP; {1,}pop REG64;{,}ROP;RET',
                'add rsp, REG64; {,}ROP; RET',
                'add rsp, [REG64 + CONST]; {,}ROP; RET',
                'add rsp, CONST; {,}ROP; RET',
                'sub rsp, REG64; {,}ROP; RET',
                'sub rsp, [REG64 + CONST]; {,}ROP; RET',
                'sub rsp, CONST; {,}ROP; RET',
                'xor rsp, REG64; {,}ROP; RET',
                'xor rsp, [REG64 + CONST]; {,}ROP; RET',],

'pops' :   ['{1,}pop REG64; RET',],
'pushes' : ['{1,}push REG64; RET',],
'read' :   ['mov REG64, [REG64]; RET'],
'write' :  ['mov [REG64], REG64; RET'],
'pushad':  ['pushad; {,}add rsp,CONST; RET'],

'regs' :   ['inc REG64;{,}pop REG64;RET',
            'dec REG64;{,}pop REG64;RET',
            'mov REG64, REG32;{,}pop REG64;RET',],
                  }
