#!/usr/bin/python2

import BeaEnginePython as Bea
from ..inteldisasdb import IntelDisasDB
from .x86_64type import X86_64Immediat, X86_64RelocableImmediat
from .x86_64instr import X86_64InstrDisas

class X86_64DisasDB(IntelDisasDB):
    Immediat = X86_64Immediat
    RelocableImmediat = X86_64RelocableImmediat
    InstrDisas = X86_64InstrDisas
    bea_archi = 64
