from ..inteltype import IntelRegister, IntelMemAccess, IntelImmediat, IntelRelocableImmediat
from ..x86.x86type import X86Register
import BeaEnginePython as Bea


class X86_64Register(IntelRegister):
    DEFAULT_REG_SIZE  = 8
    valid64 = ["RAX", "RCX", "RDX", "RBX", "RSP", "RBP", "RSI", "RDI",
                "R8", "R9", "R10", "R11", "R12", "R13", "R14", "R15"]
    valid32 = X86Register.valid32 + ["R8D", "R9D", "R10D", "R11D", "R12D", "R13D", "R14D", "R15D"]
    valid16 = X86Register.valid16 + ["R8W", "R9W", "R10W", "R11W", "R12W", "R13W", "R14W", "R15W"]
    valid8 = X86Register.valid8 + ["R8L", "R9L", "R10L", "R11L", "R12L", "R13L", "R14L", "R15L"]
    valid8 += ["DIL", "SIL", "BPL", "SPL"]

    valid_regs = dict([("NOREG", 0)])
    valid_regs.update((reg64, 8) for reg64 in valid64)
    valid_regs.update((reg32, 4) for reg32 in valid32)
    valid_regs.update((reg16, 2) for reg16 in valid16)
    valid_regs.update((reg8, 1) for reg8 in valid8)

    Bea_RegValue =  {
                 Bea.REG0: "RAX",
                 Bea.REG1: "RCX",
                 Bea.REG2: "RDX",
                 Bea.REG3: "RBX",
                 Bea.REG4: "RSP",
                 Bea.REG5: "RBP",
                 Bea.REG6: "RSI",
                 Bea.REG7: "RDI",
                 Bea.REG8  : "R8",
                 Bea.REG9  : "R9",
                 Bea.REG10 : "R10",
                 Bea.REG11 : "R11",
                 Bea.REG12 : "R12",
                 Bea.REG13 : "R13",
                 Bea.REG14 : "R14",
                 Bea.REG15 : "R15",
               }



class X86_64RelocableImmediat(IntelRelocableImmediat):
    default_size = X86_64Register.DEFAULT_REG_SIZE

class X86_64Immediat(IntelImmediat):
    default_size = X86_64Register.DEFAULT_REG_SIZE


class X86_64MemAccess(IntelMemAccess):
    valid_scale = [1, 2, 4, 8]
    Register = X86_64Register
    Immediat = X86_64Immediat
