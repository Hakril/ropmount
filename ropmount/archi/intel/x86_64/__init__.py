
import x86_64type
import x86_64instr
import x86_64disasdb
import x86_64filtreinstr
import x86_64regset

class X86_64Archi(object):
    name = "INTEL64"
    #Generic name definition
    Register = x86_64type.X86_64Register
    Immediat = x86_64type.X86_64Immediat
    RelocableImmediat = x86_64type.X86_64RelocableImmediat
    MemAccess = x86_64type.X86_64MemAccess
    RegSet = x86_64regset.X86_64RegSet
    DisasDB = x86_64disasdb.X86_64DisasDB
    filter_instrs = x86_64filtreinstr.x86_64_filter_instrs
    reg_group = x86_64filtreinstr.x86_64_reg_group
    predef_template = x86_64filtreinstr.x86_64_predef_template

archi = X86_64Archi

#Specific name definition
X86_64Register = x86_64type.X86_64Register
X86_64Immediat = x86_64type.X86_64Immediat
X86_64RelocableImmediat = x86_64type.X86_64RelocableImmediat
X86_64MemAccess = x86_64type.X86_64MemAccess
X86_64RegSet = x86_64regset.X86_64RegSet
X86_64DisasDB = x86_64disasdb.X86_64DisasDB
X86_64filter_instrs = x86_64filtreinstr.x86_64_filter_instrs
X86_64reg_group = x86_64filtreinstr.x86_64_reg_group
x86_64predef_template = x86_64filtreinstr.x86_64_predef_template
