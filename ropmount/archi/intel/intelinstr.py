#!/usr/bin/python2

from ..instr import InstrDisas
import BeaEnginePython as Bea


class IntelInstrDisas(InstrDisas):
    """Disas of a simple Instruction"""
    #used by is_valid_rop
    rop_allowed_type = [
            Bea.DATA_TRANSFER,
            Bea.ARITHMETIC_INSTRUCTION,
            Bea.LOGICAL_INSTRUCTION,
            Bea.SHIFT_ROTATE,
            Bea.BIT_BYTE,
            #Bea.CONTROL_TRANSFER,
            Bea.STRING_INSTRUCTION,
            Bea.ENTER_LEAVE_INSTRUCTION,
            Bea.FLAG_CONTROL_INSTRUCTION,
            Bea.SEGMENT_REGISTER,
            #Bea.MISCELLANEOUS_INSTRUCTION,
            Bea.COMPARISON_INSTRUCTION,
            ]

    RegSet = None
    Immediat = None
    MemAccess = None
    Register = None
    StackReg = None

    def __init__(self, disasm, size, vaddr):
        self.descr = disasm.CompleteInstr.strip()
        self.mnemo = disasm.Instruction.Mnemonic.strip()
        self.size = size
        self.vaddr = vaddr
        self.args = []
        self.implicitreg = self.RegSet([])
        for arg in [disasm.Argument1, disasm.Argument2, disasm.Argument3]:
            self.args.append(self._parse_arg(disasm, size, arg))
        self.category = disasm.Instruction.Category
        #setxx (set byte on cond) : as we don't follow exec flow
        #we can't know flags state : so setxx will invalid is arg
        if self.mnemo == 'push' or self.mnemo.startswith('set'):
            self.args[0] = self.args[1]
            self.args[1] = None
        if self.mnemo == 'ret':
            self.args[0] = self.Immediat(0, self.Immediat.default_size)
        #add implicit modified registers
        if disasm.Instruction.ImplicitModifiedRegs:
            mr = disasm.Instruction.ImplicitModifiedRegs
            if mr & 0xffff0000 == Bea.REGISTER_TYPE + Bea.GENERAL_REG:
                modregs = self.Register.listreg_from_bea(mr)
                #Of course ESP is implicitly modified : we don't care
                #ESP modification that bother us are EXPLICITE
                #(pop esp ; mov esp, 0x000, ...)
                self.implicitreg.extend([reg for reg in modregs if reg != self.StackReg])

    @classmethod
    def _parse_arg(cls, disasm, size, arg):
        argtype = arg.ArgType & 0xffff0000
        if argtype == Bea.MEMORY_TYPE:
            return cls.MemAccess.from_bea_argtype(arg)
        if argtype == Bea.MEMORY_TYPE + Bea.RELATIVE_:
            #X86_64 relative MemAccess
            arg.Memory.Displacement += disasm.VirtualAddr + size
            return cls.MemAccess.from_bea_argtype(arg)
        if argtype == Bea.REGISTER_TYPE + Bea.GENERAL_REG:
            return cls.Register.from_bea_argtype(arg)
        if argtype == Bea.CONSTANT_TYPE + Bea.ABSOLUTE_:
            return cls.Immediat(disasm.Instruction.Immediat, cls.Immediat.default_size)
        if argtype == Bea.NO_ARGUMENT:
            return None
        if argtype == Bea.CONSTANT_TYPE + Bea.RELATIVE_:
            return cls.Immediat(disasm.Instruction.AddrValue, cls.Immediat.default_size)
        #Ignored Argument : Not really useful for ROP
        #fpu or sse or MMX: ignore
        regtype = argtype - Bea.REGISTER_TYPE
        if regtype in [Bea.FPU_REG, Bea.SSE_REG, Bea.MMX_REG]:
            return -1
        #Segment Selector // MEM_MANAGE_REG : ignore
        if regtype == Bea.SEGMENT_REG or regtype == Bea.MEMORY_MANAGEMENT_REG:
            return -1
        #Eflags : Ignore for the moment
        if regtype == Bea.SPECIAL_REG:
            return -1
        #CRX/DR register
        if regtype == Bea.CR_REG or regtype == Bea.DR_REG:
            return -1
        print "[DBG] : no handled instr type : {0}".format(hex(arg.ArgType & 0xffff0000))

    def __str__(self):
        return self.descr

    def is_valid_rop(self):
        if self.category & 0xFFFF0000 != Bea.GENERAL_PURPOSE_INSTRUCTION:
            return False
        if self.category & 0xFFFF not in self.rop_allowed_type:
            return False
        #if dest is a memory type : discard
        if  isinstance(self.args[0], self.MemAccess) :
            return False
        #is source deref other than ESP : discard
        if (isinstance(self.args[1], self.MemAccess) and
                self.args[1].base != self.StackReg):
            return False
        #just because 'dec ESP' is just a pain for ROP
        if self.mnemo == 'pop':
            return isinstance(self.args[0], self.Register)
        if self.mnemo == 'dec' and self.args[0] == self.StackReg:
            return False
        if self.mnemo == 'push':
            return False
        #Add esp (REG/MemAcces) : don't know what to do : Ignore for the moment
        if (self.mnemo == 'add' and self.args[0] == self.StackReg):
            if not isinstance(self.args[1], self.Immediat):
                return False
        return True

    def is_end_rop(self):
        return self.mnemo in ['ret', 'retn', 'retf']

    def stack_consumption(self):
        if self.mnemo == 'pop':
            return self.args[0].size
        if self.mnemo == 'ret':
            return self.Register.DEFAULT_REG_SIZE
        if self.mnemo == 'retn':
            return self.Register.DEFAULT_REG_SIZE + self.args[0].value
        if self.mnemo == 'push':
            return 0 - self.args[0].size
        if (self.mnemo == 'add' and self.args[0] == self.StackReg):
            #add esp,immed
            if isinstance(self.args[1], self.Immediat):
                return self.args[1].value
            #add esp,register/ memaccess : ??
            raise NotImplementedError("Add esp,(REG/MemAcces) not implemented yet")
            #return 0xffff
        return 0

    def invalid_register(self):
        if self.mnemo == 'xchg':
            return self.RegSet(self.args[0:2]) | self.implicitreg
        #Dummy: be efficient
        if isinstance(self.args[0], self.Register):
            return  self.RegSet([self.args[0]]) | self.implicitreg
        return self.implicitreg
