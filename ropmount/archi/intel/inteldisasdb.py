from ..disasdb import DisasDB
import BeaEnginePython as Bea

class IntelDisasDB(DisasDB):
    """
        A database of InstrDisas:
        Efficient to extract a lot of GadgetDisas from a simple block of data
    """

    Immediat = None
    RelocableImmediat = None
    InstrDisas = None
    bea_archi = 0

    def __init__(self, mapping):
        code_block = mapping.get_code()
        self.vaddr = mapping.vaddr
        self.mapping = mapping
        #Add garbage at the end to disas after the code_bock limit
        self.buffer = Bea.create_string_buffer(code_block + "\x00" * 15, mapping.size + 15)
        self.buffer_addr = Bea.addressof(self.buffer)
        # Addr => FixDisasm
        if mapping.is_relocable:
            self.addr_type = lambda addr: self.RelocableImmediat(addr, self.RelocableImmediat.default_size, self.mapping)
        else:
            self.addr_type = lambda addr: self.Immediat(addr, self.Immediat.default_size)

    def disas(self, addr):
        """Return disas of instr at addr if valid : None else"""
        if not addr in self.mapping:
            raise ValueError("Disas addr not in segment limits")
        res = self._get_fixdisasm(addr)
        #If disas go after code block : ignore
        if not res or addr + res.size - 1 not in self.mapping:
            res = None
        return res

    #addr not in database
    def _get_fixdisasm(self, addr):
        """Disas instruction at addr : return None if disas failed"""
        instr = Bea.DISASM()
        instr.Archi = self.bea_archi
        instr.EIP = self.buffer_addr + (addr - self.mapping.vaddr)
        instr.VirtualAddr = addr
        size = Bea.Disasm(Bea.addressof(instr))
        if size < 0:
            return None
        return self.InstrDisas(instr, size, self.addr_type(addr))
