from ..inteltype import IntelRegister, IntelMemAccess, IntelImmediat, IntelRelocableImmediat
import BeaEnginePython as Bea


class X86Register(IntelRegister):
    DEFAULT_REG_SIZE  = 4
    valid32 = ["EAX", "ECX", "EDX", "EBX", "ESP", "EBP", "ESI", "EDI"]
    valid16 = ["AX","BX", "CX","DX", "DI", "SI", "BP", "SP"]
    valid8 =  ["AH", "AL", "BH", "BL", "CH", "CL", "DH", "DL"]

    valid_regs = dict([("NOREG", 0)])
    valid_regs.update((reg32, 4) for reg32 in valid32)
    valid_regs.update((reg16, 2) for reg16 in valid16)
    valid_regs.update((reg8, 1) for reg8 in valid8)

    Bea_RegValue =  {
             Bea.REG0: "EAX",
             Bea.REG1: "ECX",
             Bea.REG2: "EDX",
             Bea.REG3: "EBX",
             Bea.REG4: "ESP",
             Bea.REG5: "EBP",
             Bea.REG6: "ESI",
             Bea.REG7: "EDI",
           }




class X86RelocableImmediat(IntelRelocableImmediat):
    default_size = X86Register.DEFAULT_REG_SIZE

class X86Immediat(IntelImmediat):
    default_size = X86Register.DEFAULT_REG_SIZE
X86Immediat.register(X86RelocableImmediat)


class X86MemAccess(IntelMemAccess):
    valid_scale = [1, 2, 4, 8]
    Register = X86Register
    Immediat = X86Immediat
