#!/usr/bin/python2

import BeaEnginePython as Bea
from ..inteldisasdb import IntelDisasDB
from .x86instr import X86InstrDisas
from .x86type import X86Immediat, X86RelocableImmediat

class X86DisasDB(IntelDisasDB):
    Immediat = X86Immediat
    RelocableImmediat = X86RelocableImmediat
    InstrDisas = X86InstrDisas
    bea_archi = 32
