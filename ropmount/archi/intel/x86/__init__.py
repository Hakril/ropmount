import x86type
import x86instr
import x86disasdb
import x86filtreinstr
import x86regset

class X86Archi(object):
    name = "INTEL32"
    #Generic name definition
    Register = x86type.X86Register
    Immediat = x86type.X86Immediat
    RelocableImmediat = x86type.X86RelocableImmediat
    MemAccess = x86type.X86MemAccess
    RegSet = x86regset.X86RegSet
    DisasDB = x86disasdb.X86DisasDB
    filter_instrs = x86filtreinstr.x86_filter_instrs
    reg_group = x86filtreinstr.x86_reg_group
    predef_template = x86filtreinstr.x86_predef_template

archi = X86Archi

#Specific name definition
X86Register = x86type.X86Register
X86Immediat = x86type.X86Immediat
X86RelocableImmediat = x86type.X86RelocableImmediat
X86MemAccess = x86type.X86MemAccess
X86RegSet = x86regset.X86RegSet
X86DisasDB = x86disasdb.X86DisasDB
X86filter_instrs = x86filtreinstr.x86_filter_instrs
X86reg_group = x86filtreinstr.x86_reg_group
x86_predef_template = x86filtreinstr.x86_predef_template
