#!/usr/bin/python2

from ...type import Register, MemAccess, Immediat
from .x86type import X86Register

CtoM = (MemAccess, Immediat)
CtoR = (Register, Immediat)
MtoR = (Register, MemAccess)
RtoM = (MemAccess, Register)
RtoR = (Register, Register)

simple_double_args = [RtoR, CtoR, CtoM, MtoR, RtoM]
simple_double_noconst = [RtoR, MtoR, RtoM]
CorR = [(Register,), (Immediat,)]
CorRorM = [(Register,), (Immediat,), (MemAccess,)]
NoArg = [()]

#Represent the mnemonic we can filter and their arguments
#Used by asmparser in ropfinding
x86_filter_instrs  = {
        #2 args instr
        'add' : simple_double_args,
        'and' : simple_double_args,
        'mov' : simple_double_args,
        'or' : simple_double_args,
        'sub' : simple_double_args,
        'xchg' : simple_double_noconst,
        'xor' : simple_double_args,
        #0/1 arg instr
        'call': CorRorM,
        'jmp': CorRorM,
        'dec' : CorR,
        'inc' : CorR,
        'int' : [(Immediat,)],
        'neg' : [(Register,)],
        'not' : [(Register,)],
        'pop' : [(Register,)],
        'popad' : NoArg,
        'push' : CorRorM,
        'pushad' : NoArg,
        'lea' : [MtoR],
        'leave' : NoArg,
        'ret' : NoArg,
        'retn' : [(Immediat,)],
             }
x86_reg_group = {
        "REG8" : map(X86Register, X86Register.valid8),
        "REG16" : map(X86Register, X86Register.valid16),
        "REG32" : map(X86Register, X86Register.valid32),
        "REG" : map(X86Register, X86Register.valid_regs)
        }

x86_predef_template = {
'stackpivot' : ['mov esp, [REG32]; {,}ROP; RET',
                'mov esp,[REG32 + CONST]; {,}ROP; RET',
                'mov esp, REG32 ; {,}ROP; RET',
                'pop esp; {,}ROP; RET',
                'xchg esp, REG32; {,}ROP; RET',
                'xchg REG32, esp; {,}ROP; RET',
                'push REG32;{,}ROP ;pop esp; {,}ROP; RET',
                'push REG32; {,}ROP; {1,}pop REG32;{,}ROP;RET',
                'add esp, REG32; {,}ROP; RET',
                'add esp, [REG32 + CONST]; {,}ROP; RET',
                'add esp, CONST; {,}ROP; RET',
                'sub esp, REG32; {,}ROP; RET',
                'sub esp, [REG32 + CONST]; {,}ROP; RET',
                'sub esp, CONST; {,}ROP; RET',
                'xor esp, REG32; {,}ROP; RET',
                'xor esp, [REG32 + CONST]; {,}ROP; RET',],

'pops' :   ['{1,}pop REG32; RET',],
'pushes' : ['{1,}push REG32; RET',],
'read' :   ['mov REG32, [REG32]; RET'],
'write' :  ['mov [REG32], REG32; RET'],
'pushad':  ['pushad; {,}add esp,CONST; RET'],

'regs' :   ['inc REG32;{,}pop REG32;RET',
            'dec REG32;{,}pop REG32;RET',
            'mov REG32, REG32;{,}pop REG32;RET',],

'get_esp' : ['mov REG32, esp; {,}pop REG32;RET',
             'add REG32, esp; {,}pop REG32;RET',
             'sub REG32, esp; {,}pop REG32;RET',
             'xor REG32, esp; {,}pop REG32;RET',
             'or REG32, esp; {,}pop REG32;RET',
             'and REG32, esp; {,}pop REG32;RET',
             'lea REG32, [esp]; {,}pop REG32;RET',
             'lea REG32, [esp + CONST]; {,}pop REG32;RET',
             'mov [esp], esp; {1,}pop REG32;RET',
             'mov [REG32], esp; {,}pop REG32;RET',],
                  }
