#!/usr/bin/python2

from ..intelinstr import IntelInstrDisas
from .x86type import X86Immediat, X86Register, X86MemAccess, X86RelocableImmediat
from .x86regset import X86RegSet


class X86InstrDisas(IntelInstrDisas):
    RegSet = X86RegSet
    Immediat = X86Immediat
    MemAccess = X86MemAccess
    Register = X86Register
    StackReg = X86Register("ESP")
