from abc import ABCMeta

from ..type import Register, MemAccess, Immediat, RelocableImmediat
import BeaEnginePython as Bea

class IntelRegister(Register):
    DEFAULT_REG_SIZE  = 0
#   'name' : size
    valid_regs = {"NOREG" : 0}
    Bea_RegValue =  {
             Bea.REG0: "NOREG",
             Bea.REG1: "NOREG",
             Bea.REG2: "NOREG",
             Bea.REG3: "NOREG",
             Bea.REG4: "NOREG",
             Bea.REG5: "NOREG",
             Bea.REG6: "NOREG",
             Bea.REG7: "NOREG",
           }

    def __init__(self, reg):
        reg = reg.upper()
        if reg not in self.valid_regs:
            raise ValueError("Not a valid {0} : <{1}>".format(self.__class__.__name__, reg))
        self.value = reg
        self.size = self.valid_regs[reg]

    @classmethod
    def from_bea_value(cls, value):
        return cls(cls.Bea_RegValue[value])

    @classmethod
    def from_bea_argtype(cls, arg):
        if arg.ArgType & 0xffff0000 != Bea.REGISTER_TYPE + Bea.GENERAL_REG:
            raise ValueError("Register from Bea with bad ArgType")
        try:
            if arg.ArgMnemonic:
                return cls(arg.ArgMnemonic)
            if arg.ArgSize != cls.DEFAULT_REG_SIZE * 8:
                # This case is not for simple rop instruction
                return -1
            return cls.from_bea_value(arg.ArgType & 0xffff)
        except KeyError:
            # This case is not for simple rop instruction
            #print "Reg from Bea : KeyError[{0}]".format(hex(arg.ArgType & 0xffff))
            return -1

    @classmethod
    def listreg_from_bea(cls, value):
        """Extract list of conserned register from a 'or' of Bea Register Flags"""
        res = []
        if value & 0xffff0000 != Bea.REGISTER_TYPE + Bea.GENERAL_REG:
            print "FAIL CALL"
            return res
        value = value & 0xffff
        for nb in cls.Bea_RegValue:
            if value & nb:
                res.append(cls.from_bea_value(nb))
        return res

    def __len__(self):
        return self.size

    def __eq__(self, other):
        if other == None:
            return False
        if not isinstance(other, self.__class__):
            raise ValueError("Can't compare {0} and {1}".format(self.__class__, other))
        return self.value == other.value

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.value)

    def __str__(self):
        return self.value

    def __repr__(self):
        return "{0}('{1}')".format(self.__class__.__name__, self.value)





class IntelRelocableImmediat(RelocableImmediat):
    pass

class IntelImmediat(Immediat):
    __metaclass__ = ABCMeta
IntelImmediat.register(IntelRelocableImmediat)


class IntelMemAccess(MemAccess):
    valid_scale = [1, 2, 4, 8]
    Register = None
    Immediat = None

    def __init__(self, base=None, index=None, scale=0, displacement=0):
        if index and not scale or not index and scale > 1:
            raise ValueError("MemAcces with index={0} and scale={1}".format(index, scale))
        if scale and scale not in self.valid_scale:
            raise ValueError("MemAcces invalid scale : {0}".format(scale))
        self.scale = self.Immediat(scale, self.Immediat.default_size)
        self.displacement = self.Immediat(displacement, self.Immediat.default_size)

        if base:
            try:
                self.base = self.Register.from_bea_value(base)
            except KeyError:
                #If this is directly the name of the register (test purpose)
                if not base in self.Register.Bea_RegValue.values():
                    raise
                self.base = self.Register(base)
        else:
            self.base = self.Register("NOREG")
        if index:
            try:
                self.index = self.Register.from_bea_value(index)
            except KeyError:
                #If this is directly the name of the register (test purpose)
                if not index in self.Register.Bea_RegValue.values():
                    raise
                self.index = self.Register(index)
        else:
            self.index = self.Register("NOREG")

    def __repr__(self):
        attrs = ["{0}={1}".format(name, getattr(self, name).__repr__()) for
                name in ["base", "index", "scale", "displacement"] if hasattr(self, name)]
        return "{0}({1})".format(self.__class__.__name__, ", ".join(attrs))

    @classmethod
    def from_bea_argtype(cls, arg):
        if (not arg.ArgType & 0xffff0000 == Bea.MEMORY_TYPE and
                not arg.ArgType & 0xffff0000 == Bea.MEMORY_TYPE + Bea.RELATIVE_):
            raise ValueError("{0} : invalid argtype".format(cls))
        mem = arg.Memory
        return cls(mem.BaseRegister, mem.IndexRegister, mem.Scale, mem.Displacement)
