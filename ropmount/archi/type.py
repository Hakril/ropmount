#!/usr/bin/python2

class Register(object):
    def __hash__(self):
        raise NotImplementedError("{0} register need a valid __hash__ implem"
                                    .format(self.__class__))

    def __eq__(self, other):
        raise NotImplementedError("{0} need a valid __hash__ implem"
                .format(self.__class__))

    def __ne__(self, other):
        return not self == other


class MemAccess(object):
    pass

class Immediat(tuple):
    #May have check on value later
    default_size = -1
    def __new__(cls, value, size, *args, **kwags):
        if size < 0:
            raise ValueError("Immediat size < 0")
        if not isinstance(value, (long, int)):
            value = int(value, 0)
        return super(Immediat, cls).__new__(cls, (value, size))


    #TODO : change to property
    def __init__(self, value, size, *args, **kwargs):
        self.value = self[0]
        self.size = self[1]

    def dump(self):
        """
            Get the finals value of the Immediat in form
            (value, size)
            Used just for the dumping of RopStack
            Will allow non complete kind of Immediat (as relocationable gadget)
        """
        return self

    def __str__(self):
        return hex(self.value)

    def __repr__(self):
        return "{0}({1}, {2})".format(self.__class__.__name__, hex(self.value), self.size)

class RelocableImmediat(Immediat):
    """
        Represent a relocable address
        Is represented by the offset in his current mapping
    """
    def __init__(self, value, size, mapping):
        super(RelocableImmediat, self).__init__(value, size, mapping)
        self.mapping = mapping

    def dump(self):
        return Immediat(self.value + self.mapping.get_real_vaddr(), self.size)

    def __repr__(self):
        return "{0}({1},{2})".format(self.__class__.__name__, self.value, self.size)
