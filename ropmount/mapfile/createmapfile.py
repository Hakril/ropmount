#! /usr/bin/python2

import elf
import map_pe

# The list of type supported, each the tuple contains:
#  - The function to verify is the file is of Format F (param = file)
#  - The class corresponding to this format (param = file)

ExeFormat = [
        (elf.Elf32),
        (elf.Elf64),
        (map_pe.PeMapFile)
        ]

class FormatNotImplemented(Exception):
    pass

def create_mapfile(filename):
    """ Return a MapFile if type of filename is handled"""
    f = open(filename, "rb")
    for MapClass in ExeFormat:
        if (MapClass.is_valid(f)):
            return MapClass(f)
    raise FormatNotImplemented("Unknow format for file :" + filename)
