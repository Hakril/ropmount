import os.path
from .mapfile import MapFile

class MultipleMapFileFixException(Exception):
    pass

class MultipleMapFile(MapFile):
    """
        A class that 'concat' a various number of MapFile
    """

    def __init__(self, mapfiles):
        self.mapfiles = mapfiles
        for mapfile in mapfiles[1:]:
            if mapfile.archi != mapfiles[0].archi:
                raise ValueError('All files must have the same archi: get <{0}> and <{1}>'
                        .format(mapfile.archi.__name__, mapfiles[0].archi.__name__))
            if mapfile.kernel != mapfiles[0].kernel:
                raise ValueError('All files must have the same kernel: get <{0}> and <{1}>'
                        .format(mapfile.kernel.__name__, mapfiles[0].kernel.__name__))
        self.archi = mapfiles[0].archi
        self.kernel = mapfiles[0].kernel
        self.filename = self.compute_filename(mapfiles)

    @classmethod
    def compute_filename(cls, mapfiles):
        dirname = os.path.dirname(mapfiles[0].filename) or "."
        return (dirname + os.path.sep +
                "_".join([os.path.basename(mapfile.filename) for mapfile in mapfiles]))

    def get_mapping(self):
        return [mapp for mapfile in self.mapfiles for mapp in mapfile.get_mapping()]

    def get_functions(self):
        res = {}
        for mapfile in self.mapfiles:
            res.update(mapfile.get_functions())
        return res

    def get_symbols(self):
        res = {}
        for mapfile in self.mapfiles:
            res.update(mapfile.get_symbols())
        return res

    def get_real_vaddr(self, mapping):
        for mapfile in self.mapfiles:
            if mapping in mapfile.get_mapping():
                return mapfile.get_real_vaddr(mapping)
        raise ValueError('mapping no found')

    def __getitem__(self, value):
        x = [mf for mf in self.mapfiles if mf.filename == value]
        if not x:
            raise KeyError(value)
        return x[0]

    def __getstate__(self):
        return (self.mapfiles,)

    def __setstate__(self, state):
        self.mapfiles = state[0]
        self.kernel = self.mapfiles[0].kernel
        self.archi = self.mapfiles[0].archi
        self.filename = self.compute_filename(self.mapfiles)

    def fix_baseaddr(self, value):
        msg = "Try to fix_baseaddr of a MultipleMapfile: Maybe you mean one of the following sub-Mapfile:\n"
        msg += "\n".join(["'" + mapfile.filename + "'" for mapfile in self.mapfiles if mapfile.is_relocable])
        raise MultipleMapFileFixException(msg)

    def __repr__(self):
        return "{0}({1})".format(self.__class__.__name__, [m.__repr__() for m in self.mapfiles])
