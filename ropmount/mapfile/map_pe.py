#! /usr/bin/python2

import ropmount.utils.depack as depack
from .mapfile import MapFile
from .mapping import Mapping
from .extractor.PeExtractor import PeExtractor
from ropmount.mapfile.kernel import WindowsKernel


#constant
IMAGE_FILE_MACHINE_I386 = 0x014c
IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = 0x40
#Access constant
IMAGE_SCN_MEM_EXECUTE = 0x20000000
IMAGE_SCN_MEM_READ = 0x40000000
IMAGE_SCN_MEM_WRITE = 0x80000000

#type
BYTE = "B"
WORD = "H"
DWORD = "I"
LONG = "L"

#struct

DOS_HEADER = [
        ("e_magic", WORD),
        ("padind", depack.padding(WORD * 29)),
        ("e_lfanew", LONG)]

FILE_HEADER = [
        ("Machine", WORD),
        ("NbOfSections", WORD),
        ("padding", depack.padding(DWORD * 3 + 2 * WORD))]

OPTIONAL_HEADER = [
        ("Magic", WORD),
        ("padding", depack.padding(2 * BYTE)),
        ("padding", depack.padding(6 * DWORD)),
        ("ImageBase", DWORD),
        ("padding", depack.padding(2 * DWORD + 6 * WORD + 4 * DWORD )),
        ("DllCharacteristics", WORD),
        ("DllCharacteristics2", WORD),
        ("padding", depack.padding(6 * DWORD)),
        ("padding_DATADIR", depack.padding(2 * DWORD * 16))]

NT_HEADER = [
        ("Signature", DWORD),
        ("FileHeader", FILE_HEADER),
        ("OptionalHeader", OPTIONAL_HEADER)]


SECTION_HEADER = [
        ("Name", BYTE * 8),
        ("VirtualSize", DWORD),
        ("VirtualAddress", DWORD),
        ("SizeOfRawData", DWORD),
        ("PtrToRawData", DWORD),
        ("padding", depack.padding(2 * DWORD + 2 * WORD)),
        ("Characteristics", DWORD)]

class PeMapFile(MapFile):

    def __init__(self, f):
        super(PeMapFile, self).__init__(f)
        f.seek(0,0)
        res = dict()
        DosHeader = depack.depack(DOS_HEADER, f)
        f.seek(DosHeader['e_lfanew'],0)
        PeHeader = depack.depack(NT_HEADER, f)
        #Check Archi
        if not PeHeader['FileHeader']['Machine'] == IMAGE_FILE_MACHINE_I386:
            raise NotImplementedError("Pe: NotImplemented Machine Format")
        self._init_kernel(WindowsKernel)
        self._init_archi("INTEL32")
        #relocable ?
        self.is_relocable = (PeHeader['OptionalHeader']['DllCharacteristics'] &
                IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE)
        self._mapping = self.pe_map(f)
        self._extractor = PeExtractor(self)

    @classmethod
    def is_valid(cls, f):
        f.seek(0,0)
        begin = f.read(2)
        return begin == "MZ"


    def pe_map(self, f):
        f.seek(0,0)
        res = []
        DosHeader = depack.depack(DOS_HEADER, f)
        f.seek(DosHeader['e_lfanew'],0)
        PeHeader = depack.depack(NT_HEADER, f)
        base = PeHeader['OptionalHeader']['ImageBase']
        for _ in range(PeHeader['FileHeader']['NbOfSections']):
            section = depack.depack(SECTION_HEADER, f)
            res.append(self.mapping_from_sections(base, section))
        return dict(res)

    def mapping_from_sections(self, base, section):
        access_name = {IMAGE_SCN_MEM_EXECUTE: 'X', IMAGE_SCN_MEM_READ : 'R', IMAGE_SCN_MEM_WRITE : 'W'}
        access = set()
        for flag, name in access_name.items():
            if section['Characteristics'] & flag:
                access.add(name)
        mapp = Mapping(self, base + section['VirtualAddress'], section['VirtualSize'], access, self.is_relocable)
        return (mapp, (section['PtrToRawData'], section['SizeOfRawData']))

    def offset_from_vaddr(self, addr):
        for mapp, (file_off, file_size) in self._mapping.items():
            if addr in mapp:
                offset = addr - mapp.vaddr
                if offset > file_size:
                    continue
                return offset + file_off

    def get_mapping(self):
        return self._mapping.keys()





