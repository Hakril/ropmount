import hashlib
from .mapfile import MapFile
from .mapping import Mapping


class RawMapFile(MapFile):
    def __init__(self, data, archi_name):
        self._data = data
        self.extractor = None
        self.is_relocable = False
        self.filename = "RawData_" + hashlib.md5(data).hexdigest()
        self.archi_name = archi_name
        self._init_archi(archi_name)
        self._mapping = [Mapping(self, 0, len(data), set('RX'), None)]

    def read_from_vaddr(self, vaddr, size):
        return self._data[vaddr : vaddr + size]

    def get_mapping(self):
        return self._mapping

    def get_symbols(self):
        return {}

    def get_functions(self):
        return {}

    def __getstate__(self):
        return (self._data, self.archi_name)

    def __setstate__(self, state):
        self.__init__(*state)

def is_raw(f):
    return True
