
class Mapping(object):
    """
        This is the class that represent an executable segment in memory
    """
    def __init__(self, mapfile, vaddr, size, access, relocable):
        self.vaddr = vaddr
        self.size = size
        #ANY ASLR ? :D
        self.is_relocable = relocable
        self.access = access
        self._base_addr = 0
        self._mapfile = mapfile

    def get_code(self):
        return self._mapfile.read_from_vaddr(self.vaddr, self.size)

    def get_real_vaddr(self):
        if not self.is_relocable:
            return self.vaddr
        return self._mapfile.get_real_vaddr(self)

    def __contains__(self, value):
        return self.vaddr <= value < self.vaddr + self.size

    def __repr__(self):
        acc = ""
        for i in "RWX":
            if i in self.access:
                acc += i
            else:
                acc += "-"
        return "<mapping of vaddr : {0} with acces : {1}{2}>".format(hex(self.vaddr), acc,
                " | REL" if self.is_relocable else "")

class DummyMapping(Mapping):
    """
        A mapping for a simple code block
    """
    def __init__(self, code, vaddr=0):
        self.vaddr = vaddr
        self.code = code
        self.size = len(code)
        self.is_relocable = False
        self.access = set(['X'])

    def get_code(self):
        return self.code

