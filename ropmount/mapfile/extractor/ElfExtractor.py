 # !/usr/bin/python2

#POC class for extracting function

import ropmount.utils.depack as depack
from ropmount.archi.intel import X86Archi, X86_64Archi
from .extractor import Extractor
from ..mapping import DummyMapping
from ..elf.elf import *





class ElfExtractor(Extractor):
    archi = None

    def __init__(self, mapfile):
        super(ElfExtractor, self).__init__(mapfile)
        if not isinstance(mapfile, Elf):
            raise ValueError("ElfExtractor on NonEfl Mapfile")
        self.elf = mapfile

    def sym_from_rinfo(self, r_info):
        raise NotImplementedError("NOP")

    def type_from_rinfo(self, r_info):
        raise NotImplementedError("NOP")

    def get_section(self, f, idx):
        save_pos = f.tell()
        shdr = self.get_section_header(f, idx)
        f.seek(shdr['sh_offset'])
        data = f.read(shdr['sh_size'])
        f.seek(save_pos)
        return data

    def get_section_header(self, f, idx):
        save_pos = f.tell()
        f.seek(self.Ehdr['e_shoff'] + self.Ehdr['e_shentsize'] * idx)
        shdr = depack.depack(self.elf.elf_Shdr, f)
        f.seek(save_pos)
        return shdr

    def extract_section(self, f, shdr):
        save_pos = f.tell()
        f.seek(shdr['sh_offset'])
        data = f.read(shdr['sh_size'])
        f.seek(save_pos)
        return data

    def get_str(self, offset, str_section):
        return str_section[offset:str_section.find("\x00", offset)]

    def get_syms_from_symtab(self, f ,ShdrSymTab, StrSymTab):
        """Extract all syms from symtab with a name and an addr"""
        res = {}
        f.seek(ShdrSymTab['sh_offset'])
        for _ in range(ShdrSymTab['sh_size'] / ShdrSymTab['sh_entsize']):
            Sym = depack.depack(self.elf.elf_Sym, f)
            if Sym['st_name'] and Sym['st_value']:
                res[self.get_str(Sym['st_name'], StrSymTab)] = Sym['st_value']
        return res

    def get_function_from_plt(self, f, rel_type, ShdrPlt, ShdrRelPlt, ShdrDynSym, StrSym):
        #extract all dyn Symbols
        res = {}
        Syms = []
        f.seek(ShdrDynSym['sh_offset'])
        #Possible for static linked file
        if not ShdrDynSym['sh_entsize'] or not ShdrRelPlt['sh_entsize']:
            return []
        for _ in range(ShdrDynSym['sh_size'] / ShdrDynSym['sh_entsize']):
            Sym = depack.depack(self.elf.elf_Sym, f)
            Syms.append(Sym)
        res.update(dict((self.get_str(sym['st_name'], StrSym), sym['st_value']) for sym in Syms))
        #extract plt relocation {offset -> name}
        offset_to_name = {}
        f.seek(ShdrRelPlt['sh_offset'])
        for i in range(ShdrRelPlt['sh_size'] / ShdrRelPlt['sh_entsize']):
            Rel = depack.depack(rel_type, f)
            pos = self.sym_from_rinfo(Rel['r_info'])
            offset_to_name[Rel['r_offset']] = self.get_str(Syms[pos]['st_name'] , StrSym)
        #Update result with GOT addr
        res.update(dict((name + ".got", offset) for offset, name in offset_to_name.items()))
        #Disas plt and recup dyn fonction offset and name
        res.update(self.extract_plt(ShdrPlt, offset_to_name))
        return res


    # Extract PLT addr if function
    # The goal is to be able to jump on PLT and launch resolution process
    # ShdrPlt {'fields' : value}
    # offset_to_name {addr_in_got : 'symbol'}
    # return a {name : addr}

    def extract_plt(self, ShdrPlt, offset_to_name):
        Plt = self.extract_section(self.mapfile._f, ShdrPlt)
        plt_base_addr = ShdrPlt['sh_addr']
        PltMap = DummyMapping(Plt, plt_base_addr)
        #Adapt to 86 / 64
        DB_instr = self.archi.DisasDB(PltMap)
        disas_index = 0
        res = {}
        #arch dep
        while disas_index < len(Plt):
            instr = DB_instr.disas(disas_index + plt_base_addr)
            #print("INSTR = {0}", instr.descr)
            if not instr:
                disas_index += 1
                break
            if instr.mnemo == 'jmp' and isinstance(instr.args[0], self.archi.MemAccess):
                #Don't forget : MemAccess.displacement is an <Immediat>
                if instr.args[0].displacement.value in offset_to_name:
                    res[offset_to_name[instr.args[0].displacement.value]] = disas_index + plt_base_addr
                    #print("FIND : offset_to_name[instr.args[0].displacement]]")

            disas_index += instr.size
        return res

    def get_raw_symbols(self):
        f = self.mapfile._f
        f.seek(0,0)
        Ehdr = depack.depack(self.elf.elf_Ehdr, f)
        self.Ehdr = Ehdr
        StrSection = self.get_section(f, Ehdr['e_shstrndx'])
        f.seek(Ehdr['e_shoff'])
        ShdrSymTab = None
        StrSymTab = None
        ShdrRelPlt = None
        for _ in range(Ehdr['e_shnum']):
            Shdr = depack.depack(self.elf.elf_Shdr, f)
            #print(self.get_str(Shdr['sh_name'], StrSection))
            if self.get_str(Shdr['sh_name'], StrSection) == ".symtab":
                ShdrSymTab = Shdr
                StrSymTab = self.get_section(f, Shdr['sh_link'])
            if self.get_str(Shdr['sh_name'], StrSection) == ".rel.plt":
                rel_type = self.elf.elf_Rel
                ShdrRelPlt = Shdr
                ShdrDynSym = self.get_section_header(f, Shdr['sh_link'])
                StrSym = self.get_section(f, ShdrDynSym['sh_link'])
            if self.get_str(Shdr['sh_name'], StrSection) == ".rela.plt":
                rel_type = self.elf.elf_Rela
                ShdrRelPlt = Shdr
                ShdrDynSym = self.get_section_header(f, Shdr['sh_link'])
                StrSym = self.get_section(f, ShdrDynSym['sh_link'])
            if self.get_str(Shdr['sh_name'], StrSection) == ".plt":
                ShdrPlt = Shdr
        functions  = {}
        if ShdrSymTab:
            res = self.get_syms_from_symtab(f, ShdrSymTab, StrSymTab)
            functions.update(res)
        if not ShdrRelPlt or not ShdrPlt:
            print "No .plt or .rel.plt section : can't extract dyn loader functions"
            return functions
        res = self.get_function_from_plt(f,rel_type, ShdrPlt, ShdrRelPlt, ShdrDynSym, StrSym)
        functions.update(res)
        return functions


class Elf32Extractor(ElfExtractor):
    archi = X86Archi
    def sym_from_rinfo(self, r_info):
        return r_info >> 8
    def type_from_rinfo(self, r_info):
        return r_info & 0xff

class Elf64Extractor(ElfExtractor):
    archi = X86_64Archi
    def sym_from_rinfo(self, r_info):
        return r_info >> 32

    def type_from_rinfo(self, r_info):
        return r_info & 0xffffffff
