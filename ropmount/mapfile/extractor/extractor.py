#!/usr/bin/python2

class Extractor(object):
    """A class that extract function name and adresses from a file """
    def __init__(self, mapfile):
        self.mapfile = mapfile

    def get_raw_symbols(self):
        """
            Return all functions  in the mapfile
            format : {symbole_name : vaddr}
        """
        raise NotImplementedError('Bad extractor')

    def get_functions(self):
        """
            Return all symbols/ functions that are in exec segment
            {mapping : [(name,addr), (..)]}
        """
        res = {}
        syms = self.get_raw_symbols()
        exec_mappings = filter(lambda m : 'X' in m.access, self.mapfile.get_mapping())
        in_mapping = lambda addr : [mapping for mapping in exec_mappings if addr in mapping]
        for name, addr in syms.items():
            mapp = in_mapping(addr)
            if not mapp:
                continue
            if len(mapp) > 1:
                raise ValueError('Mapping overlap')
            res[mapp[0]] = res.get(mapp[0], []) + [(name, addr)]
        return res

    def get_symbols(self):
        res = {}
        syms = self.get_raw_symbols()
        mappings = self.mapfile.get_mapping()
        in_mapping = lambda addr : [mapping for mapping in mappings if addr in mapping]
        for name, addr in syms.items():
            mapp = in_mapping(addr)
            if not mapp:
                continue
            if len(mapp) > 1:
                print name,hex(addr), mapp
                raise ValueError('Mapping overlap')
            res[mapp[0]] = res.get(mapp[0], []) + [(name, addr)]
        return res
