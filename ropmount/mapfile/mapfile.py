#! /usr/bin/python2

import ropmount.archi as archi

class RelocableDumpError(Exception):
    def __init__(self, filename):
        self.filename = filename

    def __str__(self):
        return 'Trying to dump a non fixed relocable mapping <{0}>'.format(self.filename)

class MapFile(object):
    """
        Class For File mapping and data restoring from virtual addresses
    """
    def __init__(self, file):
        #Filename must be unique !
        self.filename = file.name
        self.is_relocable = False
        self._f = file
        self._mapping = []
        self._extractor = None
        self._base_addr = None
        self._init_archi("")

    def _init_archi(self, archi_name):
        self.archi = archi.archis[archi_name]

    def _init_kernel(self, kernel):
        self.kernel = kernel


    def offset_from_vaddr(self, vaddr):
        """ Return offset in file of virtual address vaddr """
        raise NotImplementedError

    def get_mapping(self):
        """
            Return list of Mapping representing valid code segment addresses
        """
        raise NotImplementedError

    def read_from_vaddr(self, vaddr, size):
        offset = self.offset_from_vaddr(vaddr)
        self._f.seek(offset)
        return self._f.read(size)

    def fix_baseaddr(self, addr):
        self._base_addr = addr

    def fix_base_from_symbol(self, symbol, addr):
        #Base addr = symbol addr - symbol offset in mapfile
        base_addr = addr - self.get_symbols()[symbol].value
        self.fix_baseaddr(base_addr)

    def get_real_vaddr(self, mapping):
        if self._base_addr is None:
            raise RelocableDumpError(self.filename)
        return self._base_addr + mapping.vaddr

    def _create_symbol_dict(self, syms):
        res = {}
        for mapping in syms:
            if mapping.is_relocable:
                immed = lambda addr : self.archi.RelocableImmediat(addr,
                        self.archi.RelocableImmediat.default_size, mapping=mapping)
            else:
                immed = lambda addr : self.archi.Immediat(addr,
                        self.archi.Immediat.default_size)
            res.update(dict((name, immed(addr)) for name,addr in syms[mapping]))
        return res

    def get_functions(self):
        funcs = self._extractor.get_functions()
        return self._create_symbol_dict(funcs)

    def get_symbols(self):
        syms = self._extractor.get_symbols()
        return self._create_symbol_dict(syms)

    def __repr__(self):
        relocable = ", relocable" if self.is_relocable else ""
        return "<{0} [{1}{2}] object at {3}>".format(self.__class__.__name__, self.archi.name, relocable, hex(id(self)))

    def __getstate__(self):
        return (self.filename, self.kernel, self.archi.name, self._mapping, self._extractor, self._base_addr)

    def __setstate__(self, state):
        self.filename = state[0]
        self._init_kernel(state[1])
        self._init_archi(state[2])
        self._mapping = state[3]
        self._extractor = state[4]
        self._base_addr = state[5]
        self._f = open(self.filename, 'rb')
