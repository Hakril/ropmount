#! /usr/bin/python2

from .elf import *
from ..mapfile import MapFile
from ..extractor.ElfExtractor import Elf32Extractor


#Look at elf.py : code is here

class Elf32(Elf):
    elf_Ehdr = gen_ehdr(T32ADDR, T32OFF)
    elf_Phdr = [
            ("p_type", T32WORD),
            ("p_offset", T32OFF),
            ("p_vaddr", T32ADDR),
            ("p_paddr", T32ADDR),
            ("p_filesz", T32WORD),
            ("p_memsz", T32WORD),
            ("p_flags", T32WORD),
            ("p_align", T32WORD)]
    elf_Shdr = gen_shdr(T32ADDR, T32OFF, T32WORD)
    elf_Sym = [
            ("st_name", T32WORD),
            ("st_value", T32ADDR),
            ("st_size", T32WORD),
            ("st_info", TUchar),
            ("st_other", TUchar),
            ("st_shndx", T32HALF)]
    elf_Rel = gen_rel(T32ADDR, T32WORD)
    elf_Rela = gen_rela(T32ADDR, T32WORD, T32SWORD)

    archi_name = {EM_386 : "INTEL32"}
    extractor_class = Elf32Extractor


    @classmethod
    def is_valid(cls, f):
        return cls.is_elf(f) and cls.is_32bit(f)

    @classmethod
    def is_32bit(cls, f):
        f.seek(4,0)
        t = f.read(1)
        return ord(t) == ELFCLASS32
