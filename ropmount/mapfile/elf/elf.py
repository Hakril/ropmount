
import ropmount.utils.depack as depack
from ..mapfile import MapFile
from ..mapping import Mapping
from ropmount.mapfile.kernel import UnixKernel

#Constant
IE_NIDENT = 16

# ei_class
ELFCLASSNONE = 0x0
ELFCLASS32 = 0x1
ELFCLASS64 = 0x2

PT_LOAD = 1
PF_X = (1 << 0)
PF_W = (1 << 1)
PF_R = (1 << 2)

#e_machine
EM_386 = 0x3
EM_X86_64 = 62

#ei_osabi

ELFOSABI_SYSV = 0
ELFOSABI_NETBSD = 2
ELFOSABI_LINUX = ELFOSABI_GNU = 3
ELFOSABI_FREEBSD = 9
ELFOSABI_OPENBSD = 12

#ELF type

ET_EXEC = 2
ET_DYN = 3

#Type

T32ADDR = "I"
T32HALF = "H"
T32OFF = "I"
T32SWORD = "i"
T32WORD = "I"
T64ADDR = "Q"
T64OFF = "Q"
T64XWORD = "Q"
T64XSWORD = "q"
TUchar = "B"



def gen_ehdr(TNADDR, TNOFF):
    elf_EIdent = [
            ("ei_mag" , "I"),
            ("ei_class" , TUchar),
            ("ei_data" , TUchar),
            ("ei_version" , TUchar),
            ("ei_osabi" , TUchar),
            ("ei_osabiversion" , TUchar),
            ("pad" , "x" * (IE_NIDENT - 4 - (1 * 5)))]

    elf_Ehdr = [
            ("e_ident", elf_EIdent), #Others bytes doesn't matter : Discard
            ("e_type", T32HALF),
            ("e_machine", T32HALF),
            ("e_version", T32WORD),
            ("e_entry", TNADDR),
            ("e_phoff", TNOFF),
            ("e_shoff", TNOFF),
            ("e_flags", T32WORD),
            ("e_ehsize", T32HALF),
            ("e_phentsize", T32HALF),
            ("e_phnum", T32HALF),
            ("e_shentsize", T32HALF),
            ("e_shnum", T32HALF),
            ("e_shstrndx", T32HALF)]
    return elf_Ehdr

def gen_shdr(TNADDR, TNOFF, TNXWORD):
    elf_Shdr = [
        ("sh_name", T32WORD),
        ("sh_type", T32WORD),
        ("sh_flags", TNXWORD),
        ("sh_addr", TNADDR),
        ("sh_offset", TNOFF),
        ("sh_size", TNXWORD),
        ("sh_link", T32WORD),
        ("sh_info", T32WORD),
        ("sh_addralign", TNXWORD),
        ("sh_entsize", TNXWORD)]
    return elf_Shdr

##Nop : diff : depends on archi
#    elf32_Sym = [
#            ("st_name", T32WORD),
#            ("st_value", T32ADDR),
#            ("st_size", T32WORD),
#            ("st_info", TUchar),
#            ("st_other", TUchar),
#            ("st_shndx", T32HALF)]
#    return

def gen_rel(TNADDR, TNXWORD):
    elf_Rel = [
            ("r_offset",TNADDR),
            ("r_info",TNXWORD)]
    return elf_Rel

def gen_rela(TNADDR, TNXWORD, TNXSWORD):
    elf_Rela = [
            ("r_offset",TNADDR),
            ("r_info",TNXWORD),
            ("r_addend",TNXSWORD)]
    return elf_Rela




class Elf(MapFile):
    #Fill using gen_ehdr
    elf_Ehdr = None
    #Fill by hand : order change with 32/64
    elf_Phdr = None
    elf_Shdr = None 
    #Fill by hand : order change with 32/64
    elf_Sym = None
    elf_Rel = None
    elf_Rela = None

    #e_machine -> string
    archi_name = {}
    #Will change when meaningful diff
    osabi = {
            ELFOSABI_SYSV : UnixKernel,
            ELFOSABI_NETBSD : UnixKernel,
            ELFOSABI_LINUX : UnixKernel,
            ELFOSABI_FREEBSD : UnixKernel,
            ELFOSABI_OPENBSD : UnixKernel
            }
    extractor_class = None

    @classmethod
    def is_valid(cls, f):
        raise NotImplementedError("Can't use Elf : try Elf32 ou Elf64")

    @classmethod
    def is_elf(cls, f):
        f.seek(0,0)
        begin = f.read(4)
        return begin == "\x7fELF"

    def __init__(self, f):
        super(Elf, self).__init__(f)
        f.seek(0,0)
        x = UnixKernel
        Ehdr = depack.depack(self.elf_Ehdr, f)
        self.is_relocable = Ehdr['e_type'] == ET_DYN
        if Ehdr['e_machine'] not in self.archi_name:
            raise NotImplementedError("{0} : Not Intel Archi".format(self.__class__))
        if Ehdr['e_ident']['ei_osabi'] not in self.osabi:
            raise NotImplementedError("{0} : Unknow osabi <{1}>".format(self.__class__, Ehdr['e_ident']['ei_osabi']))
        self._init_kernel(self.osabi[Ehdr['e_ident']['ei_osabi']])
        self._init_archi(self.archi_name[Ehdr['e_machine']])
        self._mapping = self.elf_map(f)
        self._extractor = None
        if self.extractor_class:
            self._extractor = self.extractor_class(self)

    def elf_map(self, f):
        res = []
        f.seek(0,0)
        Ehdr = depack.depack(self.elf_Ehdr, f)
        f.seek(Ehdr['e_phoff'])
        for _ in range(Ehdr['e_phnum']):
            Phdr = depack.depack(self.elf_Phdr, f)
            if Phdr['p_type'] == PT_LOAD:
                res.append(Phdr)
        return dict(map(self.mapping_from_phdr, res))

    def mapping_from_phdr(self, phdr):
        access_name = {PF_X : 'X', PF_R : 'R', PF_W : 'W'}
        access = set()
        for flag, name in access_name.items():
            if phdr['p_flags'] & flag:
                access.add(name)
        mapp = Mapping(self, phdr['p_vaddr'], phdr['p_memsz'], access, self.is_relocable)
        file_offset = phdr['p_offset']
        file_size = phdr['p_filesz']
        return (mapp, (file_offset, file_size))

    def offset_from_vaddr(self, addr):
        for mapp, (file_off, file_size) in self._mapping.items():
            if addr in mapp:
                offset = addr - mapp.vaddr
                if offset > file_size:
                    continue
                return offset + file_off

    def get_mapping(self):
        return self._mapping.keys()
