#! /usr/bin/python2

import ropmount.utils.depack as depack
from .elf import *
from ..mapfile import MapFile
from ..mapping import Mapping
from ..extractor.ElfExtractor import Elf64Extractor


#Look at elf.py : code is here

class Elf64(Elf):
    elf_Ehdr = gen_ehdr(T64ADDR, T64OFF)

    elf_Phdr = [
            ("p_type", T32WORD),
            ("p_flags", T32WORD),
            ("p_offset", T64OFF),
            ("p_vaddr", T64ADDR),
            ("p_paddr", T64ADDR),
            ("p_filesz", T64XWORD),
            ("p_memsz", T64XWORD),
            ("p_align", T64XWORD)]

    elf_Shdr = gen_shdr(T64ADDR, T64OFF, T64XWORD)

    elf_Sym = [
            ("st_name", T32WORD),
            ("st_info", TUchar),
            ("st_other", TUchar),
            ("st_shndx", T32HALF),
            ("st_value", T64ADDR),
            ("st_size", T64XWORD)]

    elf_Rel = gen_rel(T64ADDR, T64XWORD)
    elf_Rela = gen_rela(T64ADDR, T64XWORD, T64XSWORD)

    archi_name = {EM_X86_64 : "INTEL64"}
    extractor_class = Elf64Extractor


    @classmethod
    def is_valid(cls, f):
        return cls.is_elf(f) and cls.is_64bit(f)

    @classmethod
    def is_64bit(cls, f):
        f.seek(4,0)
        t = f.read(1)
        return ord(t) == ELFCLASS64
