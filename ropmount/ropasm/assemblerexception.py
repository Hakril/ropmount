#!/usr/bin/python2

class AssemblerException(Exception):
    """
        Exception raised by RopAssembler and RopInstr
    """
