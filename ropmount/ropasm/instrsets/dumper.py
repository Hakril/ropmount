import struct

class StackDumperException(Exception):
    pass


class StackDumper(object):
    mode = ""
    @classmethod
    def dump(cls, stackdump, mode='raw'):
        for dumper in StackDumper.__subclasses__():
            if dumper.mode == mode:
                return dumper.dump(stackdump)
        raise StackDumperException('Unknow dump mode : <{0}>'.format(mode))


class RawStackDumper(StackDumper):
    mode = 'raw'
    format_struct = { 8 : "Q",
                      4:  "I",
                      2 :" H",
                      1 : "B"}
    @classmethod
    def dump(cls, stackdump):
        res = ""
        for value, size in stackdump:
            res += struct.pack("<" + cls.format_struct[size], value)
        return res


class PythonStackDumper(StackDumper):
    mode = 'python'
    format_struct = { 8 : "Q",
                      4:  "I",
                      2 :" H",
                      1 : "B"}
    @classmethod
    def dump(cls, stackdump):
        res = """import struct\nres = ""\n"""

        for value, size in stackdump:
            res += 'res += struct.pack("<{0}", {1})\n'.format(cls.format_struct[size], value)
        return res
