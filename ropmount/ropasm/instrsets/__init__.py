import x86_instrset
import x86_64_instrset

instrsets = [x86_instrset, x86_64_instrset]
all = dict(((i.name, i) for i in instrsets))

