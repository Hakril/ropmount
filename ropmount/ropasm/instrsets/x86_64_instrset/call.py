#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86_64 import X86_64Register, X86_64Immediat
from ropmount.ropasm.assemblerexception import AssemblerException

class Call(RopInstr):
    """
        Call a function with given args
        Also clean the stack
    """
    mnemo = "call"
    templates = []
    args = (str,) + (X86_64Immediat,) * 6
    args_doc = ("function name",) + tuple("arg" + str(i) for i in range(6))
    args_min = 1
    register_order = [X86_64Register(reg) for reg in ["RDI", "RSI", "RDX", "RCX", "R8", "R9"]]

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        nb_args = len(args) - 1
        addr = cls.get_function_addr(ass, args[0])
        #No args
        if not nb_args:
            return Call(args, [], addr)

        setters = []
        #Dummy version : adapt order for better results
        for reg, value in zip(cls.register_order, args[1:]):
            setter = ass.raw_assemble("set", (reg, value), no_regs)
            if not setter:
                return None
            no_regs.add(reg)
            setters.append(setter)
        return Call(args, setters ,addr)


    @classmethod
    def get_function_addr(self, ass, function):
        if function in ass.functions:
            addr = ass.functions[function]
        else:
            #direct addr ?
            try:
                addr = int(function, 0)
                addr = X86_64Immediat(addr, X86_64Immediat.default_size)
            except ValueError:
                raise AssemblerException('unknow function <{0}>'.format(function))
        return addr


    def __init__(self, args, args_setter, func_addr):
        super(Call, self).__init__()
        self.name = args[0]
        f_args = args[1:]
        for setter in args_setter:
            self.merge(setter)
        self.stack.append(func_addr)
        self.debug = "call {0} <{1}>:".format(self.name, ",".join(map(str,f_args)))
        self.debug += "\n  " + "\n  ".join(str(setter) for setter in args_setter) + "\n  {0}:{1}".format(args[0], func_addr)

    def __str__(self):
        return self.debug
