
from itertools import permutations, combinations

from .call import Call
from ..ropinstr import RopInstr, MultipleInstr, BestLen, ImmedOrReg, RopStack
from ropmount.archi.intel.x86 import X86Register, X86RegSet, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException

class JitCallException(Exception):
    pass


class JitCall(Call):
    """
        Juste a JitCall Test
    """
    mnemo = "jitcall"
    templates = ['ret']
    args = (str,) + (ImmedOrReg,) * 6
    args_min = 2


    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        func_addr = cls.get_function_addr(ass, args[0])
        nb_args = len(args) - 1
        #Get all registers used in call
        used_register = X86RegSet()
        for arg in args[1:]:
            if isinstance(arg, X86Register):
                used_register.add(arg)
        #list of forbiden registers for jitcall
        forbid_reg = used_register | no_regs

        #Find a reg to get esp into
        usable_register = set(map(X86Register, X86Register.valid32)) - forbid_reg - X86RegSet([X86Register('ESP')])
        for reg, save_reg in [(X86Register('EDX'), X86Register('EBP'))]: #   permutations(usable_register, 2):
            #Value that can't be changed at all during jitcall write
            no_regs_jitcall = no_regs | X86RegSet([save_reg, reg])
            #Value that can't be changed at all before args write
            forbid_reg_jitcall = no_regs_jitcall | forbid_reg
            #We will not override current stack because write_take + add 4 take at least 8 bytes
            get_esp = ass.raw_assemble('mov', (reg, X86Register('ESP')), forbid_reg)
            save_esp = ass.raw_assemble('cpy', (save_reg, reg), forbid_reg)
            add_reg_4 = ass.raw_assemble('add',
                    (reg, X86Immediat(X86Register.DEFAULT_REG_SIZE, X86Immediat.default_size)), no_regs | X86RegSet([save_reg]))
            if not get_esp or not add_reg_4 or not save_esp:
                print "no get_esp or add_reg_4 or save_esp"
                continue
            dummy_write = ass.raw_assemble('write', (reg, X86Immediat(0x42424242, X86Immediat.default_size)), forbid_reg_jitcall)
            if not dummy_write:
                print "no dummy write.."
                continue
            #Search for the write call
            write_faddr = ass.raw_assemble('write', (reg, func_addr), forbid_reg_jitcall)
            if not write_faddr:
                print "jitcall : no write faddr or no write clean"
                continue
            #We begin to write the dummy stack
            #We use dummy_write to figure out the size of the real stack size
            write_jitcall = MultipleInstr()
            write_jitcall.merge(get_esp)
            write_jitcall.merge(save_esp)
            write_jitcall.merge(write_faddr)
            write_jitcall.merge(add_reg_4)
            write_jitcall.merge(dummy_write) #will be replace by stack clean
            write_jitcall.merge(add_reg_4)
            #Update used_register when finaly writed on stack ?
            write_args = MultipleInstr()
            for arg in args[1:]:
                write_arg = ass.raw_assemble('write', (reg, arg), forbid_reg_jitcall)
                if not write_arg:
                    print 'jitcall : can\'t write arg : {0}'.format(arg)
                    continue
                write_args.merge(write_arg)
                write_args.merge(add_reg_4)
            write_jitcall.merge(write_args)
            #Here all the jitcall is writed !
            #Need to -> write return value
            #        -> jump on it
            # Just need to respect no_regs

            #get write stack size
            #if change ESP pop values after : need to sub on ret value :D
            change_esp = cls.get_esp_setter(ass, save_reg, no_regs)
            if not change_esp:
                print "jitcall : no change ESP"
                continue
            write_jitcall.merge(change_esp)
            #Calc the complete stack_size for writing the jitcall
            write_jitcall_size = len(write_jitcall)
            # 1 -> function addr
            # 1 -> cleaner addr
            # 1  -> final ret
            diff = write_jitcall_size - ((1 + 1 + 1) * X86Register.DEFAULT_REG_SIZE)
            if (diff % X86Register.DEFAULT_REG_SIZE):
                print "jitcall diff not a multiple of 4..."
                continue
            stack_return = ass.raw_assemble('clean', (X86Immediat(diff / X86Register.DEFAULT_REG_SIZE, X86Immediat.default_size),), no_regs | X86RegSet([X86Register('EAX')]))
            if not stack_return:
                print "jitcall No stack return"
                continue
            print "Found cleaner = {0}".format(stack_return)
            last_clean_write = ass.raw_assemble('write', (reg, stack_return.stack.values[0]), forbid_reg_jitcall)
            if not last_clean_write:
                print "jitcall No stack return WRITE"
                continue

#            write_jitcall.merge(last_clean_write)
#            write_jitcall.merge(change_esp)

#Real write
            write_jitcall = MultipleInstr()
            write_jitcall.merge(get_esp)
            write_jitcall.merge(save_esp)
            write_jitcall.merge(write_faddr)
            write_jitcall.merge(add_reg_4)
            write_jitcall.merge(last_clean_write) #will be replace by stack clean
            write_jitcall.merge(add_reg_4)
            #Update used_register when finaly writed on stack ?
            write_jitcall.merge(write_args)
            write_jitcall.merge(change_esp)


            if len(stack_return) > X86Register.DEFAULT_REG_SIZE:
                print "need padding : {0}".format(hex(len(stack_return) - X86Register.DEFAULT_REG_SIZE))
                write_jitcall.stack.add_garbage(len(stack_return) - X86Register.DEFAULT_REG_SIZE)

            return write_jitcall

    @classmethod
    def get_esp_setter(cls, ass, src, no_regs):
        change_esp = ass.raw_assemble('mov', (X86Register('ESP'), src), no_regs)
        if not change_esp:
            return None
        #If any stack after the mov to esp is effectiv : this is garbage on jitcall stack
        #need to sub value to save_reg
        if change_esp.stack[change_esp.mov_done:]:
            to_sub = len(change_esp.stack[change_esp.mov_done:])
            if RopStack.next_addr in change_esp.stack[change_esp.mov_done:]:
                #To handle this case we need to write another 'ret' addr over saved_esp - retn value
                #So the retn will ret on a 'ret' that will trigger the current call
                return None
            sub_save = ass.raw_assemble('add', (src, X86Immediat(-to_sub & 0xffffffff, X86Immediat.default_size)), no_regs)
            if not sub_save:
                return None
            new_change_esp = MultipleInstr()
            new_change_esp.merge(sub_save)
            new_change_esp.stack.merge(change_esp.stack[:change_esp.mov_done])
            new_change_esp.debug += str(change_esp).replace("\n","\n  ")
            change_esp = new_change_esp
        return change_esp

    def __init__(self, prev, set_edi, pushad):
        super(JitCall, self).__init__()
        self.debug = "Jitcall"

    def __str__(self):
        return  self.debug
