#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86 import X86Immediat

class RetOn(RopInstr):
    """
        return on a given addr
    """
    mnemo = "reton"
    templates = []
    args = (X86Immediat,)
    args_doc = ("addr to ret on",)
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(RetOn, cls).assemble(ass, args, no_regs)
        return RetOn(args[0])

    def __init__(self, addr):
        super(RetOn, self).__init__()
        self.debug = "Ret on {0}".format(addr)
        self.stack.append(addr)

    def __str__(self):
        return self.debug
