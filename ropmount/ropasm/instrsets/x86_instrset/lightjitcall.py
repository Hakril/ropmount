
from .jitcall import JitCall
from .call import Call
from ..ropinstr import RopInstr, MultipleInstr, BestLen, ImmedOrReg, RopStack
from ropmount.archi.intel.x86 import X86Register, X86RegSet, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException


class LightJitCall(JitCall):
    """
        Juste a light JitCall Test
        LightJitcall is a JitCall (call with registers based on pushad;ret)
    """
    mnemo = "ljitcall"
    templates = ['pushad;ret', 'pushad;{,}ROP;ret']
    args = (str,) + (ImmedOrReg,) * 6
    args_min = 2
    reg_order = map(X86Register, ['EDI', 'ESI', 'EBP', 'ESP', 'EBX', 'EDX','ECX', 'EAX'])

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        if not cls.gadgets[0]:
            return None
        if SimpleLightJitCall.is_usable(ass, args[1:], no_regs):
            return SimpleLightJitCall.assemble(ass, args, no_regs)
        if AdvanceLightJitCall.is_usable(ass, args[1:], no_regs):
            return AdvanceLightJitCall.assemble(ass, args, no_regs)


class SimpleLightJitCall(LightJitCall):
    """ EDI -> function adr
        ESI -> cleaner
        EBP -> Arg1
    """

    # HowTo !
    # Find a pushad;ret
    # Set register EDI to func addr
    # mov arg1 register to EBP (register value)
    # Set ESI to cleaner 6 DWORD or more

    @classmethod
    def is_usable(cls, ass, call_args, no_regs):
         if len(call_args) > 1:
             return False
         return not any([reg in no_regs for reg in map(X86Register, ['EDI', 'ESI', 'EBP'])])

    @classmethod
    def assemble(cls, ass, args, no_regs):
        faddr = cls.get_function_addr(ass, args[0])
        call_args = args[1:]
        #Clean pushad size - 2 (EDI and ESI) and protect EAX
        cleaner = ass.raw_assemble('clean', (X86Immediat(len(cls.reg_order) - 2, X86Immediat.default_size),), no_regs | X86RegSet([X86Register('EAX')]))
        if not cleaner:
            print "LJitcall : no cleaner"
            return None
        #Set good values for the call
        set_cleaner = ass.raw_assemble('set', (X86Register('ESI'), cleaner.stack.values[0]), no_regs)
        set_faddr = ass.raw_assemble('set', (X86Register('EDI'), faddr), no_regs)
        if isinstance(args[1], X86Immediat):
            print "Seriously... Jitcall ? All you need is a simple call !"
            return None
        #Setting ebp to good value
        set_arg = ass.raw_assemble('mov', (X86Register('EBP'), args[1]), no_regs)
        if not set_arg:
            #Cannot mov src Reg to EBP
            return None
        res = MultipleInstr()
        res.merge(set_arg)
        res.merge(set_faddr)
        res.merge(set_cleaner)
        return LightJitCall(res, cleaner, cls.gadgets[0][0])

    def __init__(self, setter, cleaner, pushad):
        super(Call, self).__init__()
        self.merge(setter)
        self.stack.append_gadget(pushad, [])
        #Add garbage of cleaner if bigger than needed
        self.stack.merge(cleaner.stack[1:])
        self.debug = "LJitcall\n" + str(setter) + "\n  pushad; ret"



class AdvanceLightJitCall(LightJitCall):
    """ EBX -> function adr
        EDX -> cleaner
        ECX -> Arg1
        EAX -> Arg2
        Stack -> Other args (cannot mov)
    """

    # HowTo !
    # Find a pushad;ret
    # Set register EDI to func addr
    # mov arg1 register to EBP (register value)
    # Set ESI to cleaner 6 DWORD or more

    @classmethod
    def is_usable(cls, ass, call_args, no_regs):
        #Args on stack cannot be Registers
        if any([isinstance(arg, X86Register) for arg in call_args[2:]]):
            return False
        #Need thoss registers to LightJitCall
        return not any([reg in no_regs for reg in map(X86Register, ['EBX', 'EDX', 'ECX', 'EAX'])])


    @classmethod
    def assemble(cls, ass, args, no_regs):
        faddr = cls.get_function_addr(ass, args[0])
        call_args = args[1:]
        pushad_set, pushad = cls.find_good_pushad(ass, no_regs)
        cleaner = ass.raw_assemble('clean', (X86Immediat(max(len(call_args), 2), X86Immediat.default_size),), no_regs | X86RegSet([X86Register('EAX')]))
        set_cleaner = ass.raw_assemble('set', (X86Register('EDX'), cleaner.stack.values[0]), no_regs)
        set_faddr = ass.raw_assemble('set', (X86Register('EBX'), faddr), no_regs)
        setters = []
        for arg,reg in zip(call_args, cls.reg_order[-2:]):
            if isinstance(arg, X86Register):
                setter = ass.raw_assemble('mov', (reg, arg), no_regs)
            else:
                setter = ass.raw_assemble('set', (reg, arg), no_regs)
            if not setter:
                print "LJitcall : cannot set arg"
                return None
            setters.append(setter)
        res = MultipleInstr()
        for setter in setters:
            res.merge(setter)
        res.merge(set_faddr)
        res.merge(set_cleaner)
        for arg in call_args[2:]:
            res.stack.append(arg)
        res.merge(pushad_set)
        return AdvanceLightJitCall(res, cleaner, pushad)


    @classmethod
    def find_good_pushad(cls, ass, no_regs):
        "Find a pushad thar will ret on EBX"
        #dummy implem
        cleaner = ass.raw_assemble('clean', (X86Immediat(3, X86Immediat.default_size),), no_regs | X86RegSet([X86Register('EAX')]))
        setter = ass.raw_assemble('set', (X86Register('EDI'), cleaner.stack.values[0]), no_regs)
        return (setter, cls.gadgets[0][0])

    def __init__(self, setter, cleaner, pushad):
        super(Call, self).__init__()
        self.merge(setter)
        self.stack.append_gadget(pushad, [])
        #Add garbage of cleaner if bigger than needed
        self.stack.merge(cleaner.stack[1:])
        self.debug = "LJitcall\n" + str(setter) + "\n  pushad; ret"

    def __str__(self):
        return  self.debug
