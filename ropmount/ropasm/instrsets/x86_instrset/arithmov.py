#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86 import X86Register, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException
from .mov import Mov


class ArithMov(Mov):
    """
        Mov a register to another : it doesn't preserve the source register
        Based on arithmetic instruction (xor/ and/ ..)
    """
    mnemo = "armov"
    templates = ['add REG32,REG32;{,}ROP;RET',
                 'or REG32,REG32;{,}ROP;RET',
                 'and REG32,REG32;{,}ROP;RET',
                 'xor REG32,REG32;{,}ROP;RET']
    formats = ['add {0},{1};{{,}}ROP;RET',
               'or {0},{1};{{,}}ROP;RET',
               'and {0},{1};{{,}}ROP;RET',
               'xor {0},{1};{{,}}ROP;RET']

    def_values = {
                    'add' : 0,
                    'and' : 0xffffffff,
                    'or' : 0,
                    'xor' : 0,
                 }

    args = (X86Register, X86Register)
    args_doc = ("Dest", "Src")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        RopInstr.assemble(ass, args, no_regs)
        if args[0] in no_regs:
            raise AssemblerException("mov into forbiden register <{0}>".format(args[0]))
        if args[0] == X86Register('ESP'):
            #moving to esp is a really bad idea : need to pop esp some bad value
            return None
        gadgets = sum(
                    map(
                     lambda x : ass.specialize(cls.gadgets[x], cls.formats[x].format(args[0], args[1])),
                        range(len(cls.gadgets))), [])
        res = {}
        for armov in gadgets:
            if cls.is_valid(armov, args[0], no_regs):
                mnemo = armov.instrs[0].mnemo
                set_value = ass.raw_assemble("set", (args[0], X86Immediat(cls.def_values[mnemo], X86Immediat.default_size)), no_regs)
                if set_value:
                    mov = ArithMov(set_value, armov, args)
                    res[len(mov.stack)] = mov

        if res:
            return res[min(res)]

    def __init__(self, set_value, armov, args):
        RopInstr.__init__(self)
        mnemo = armov.instrs[0].mnemo
        self.src = args[1]
        self.dst = args[0]
        self.merge(set_value)
        self.mov_done = len(self.stack.values) + 1
        self.stack.append_gadget(armov, [])
        armov_str = "{0} {1}, {2} : <{3}>".format(mnemo, args[0], args[1], armov.descr)
        self.debug = "mov {0}, {1}:\n  {2}\n  {3}".format(args[0], args[1], str(set_value), armov_str)

    def __str__(self):
        return self.debug

