#!/usr/bin/python2

from ..ropinstr import RopInstr, BestLen
from ropmount.archi.intel.x86 import X86Register, X86RegSet
from ropmount.ropasm.assemblerexception import AssemblerException
from .mov import Mov


class PushPopMov(Mov):
    """
        Mov a register to another : it doesn't preserve the source register
        based on push;pop
    """
    mnemo = "ppmov"
    templates = ["push REG32; {,}ROP; {1,}pop REG32;{,}ROP;RET"]
    formats = ["push {0}; {{,}}ROP; {{1,}}pop {1};{{,}}ROP;RET"]
    args = (X86Register, X86Register)
    args_doc = ("Dest", "Src")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        RopInstr.assemble(ass, args, no_regs)
        no_regs_dest = X86RegSet([args[0]]) | no_regs
        if args[0] in no_regs:
            raise AssemblerException("PushPopMov in forbiden register : <{0}>".format(args[0]))
        all_ppmovs = ass.specialize(cls.gadgets[0], cls.formats[0].format(args[1], args[0]))
        #suppress all gadget that invalid a no_register
        all_ppmovs = filter(lambda gadget : not (gadget.invalid_register() & no_regs), all_ppmovs)
        # So we are at the limit of the dumper syntax
        # with the specialized template "push esp; {,}ROP; {1,}pop edx;{,}ROP;RET"
        # We will match:
        #       push esp; pop ecx; pop edx; ret
        #and    push esp; pop edx; pop ecx; ret
        # So we can't rely on ONE specialize to know which register is the real dest
        # Solution iteration of type {x,x}ROP on the first one of the syntax

        #push,pop without ROP between
        ppmovs = ass.specialize(all_ppmovs, "push {0}; pop {1}; {{,}}ROP;RET".format(args[1], args[0]))
        best = BestLen(None)
        for ppmov in ppmovs:
            if ppmov.invalid_register(2) & no_regs_dest:
                all_ppmovs.remove(ppmov)
                continue
            best.value = PushPopMov(ppmov, args)
            all_ppmovs.remove(ppmov)

        # if we are are :  we didn't find any 'push src; pop dst'
        # so we need to discard all 'push;pop' from all_ppmovs
        all_ppmovs = filter(lambda gadget : gadget.instrs[1].mnemo != 'pop', all_ppmovs)
        i = 0
        #BEURK : WORKING BUT FIXME
        #Used to generate another valid format string using '{' we need to escape
        formats = "push {0}; {{{{{{0}},{{0}}}}}}ROP; {{{{1,}}}}pop REG32;{{{{,}}}}ROP;RET".format(args[1], args[0])

        while all_ppmovs:
            i += 1
            fs = formats.format(i)
            ppmovs = ass.specialize(all_ppmovs, fs)
            for ppmov in ppmovs:
                #Is this the first get of the gadget?
                if ppmov.instrs[i + 1].mnemo == 'pop':
                    if ppmov.instrs[i + 1].args[0] != args[0]:
                        all_ppmovs.remove(ppmov)
                        continue
                    if ppmov.invalid_register(i + 2) & no_regs_dest:
                        all_ppmovs.remove(ppmov)
                        continue
                    best.value = PushPopMov(ppmov, args)
                    all_ppmovs.remove(ppmov)
        return best.value


    def __init__(self, ppmov,  args):
        RopInstr.__init__(self)
        self.src = args[1]
        self.dst = args[0]
        self.stack.append_gadget(ppmov, [])
        self.mov_done = 1
        self.debug = "ppmov {0}, {1} : <{2}>".format(args[0], args[1], ppmov.descr)

    def __str__(self):
        return self.debug
