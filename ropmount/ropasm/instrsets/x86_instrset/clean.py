#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86 import X86Register, X86Immediat


class Clean(RopInstr):
    """
        Clean X dword on the stack
    """
    mnemo = "clean"
    templates = ['{,}add esp,CONST;{1,}pop REG32;ret', 'add esp, CONST;ret']
    args = (X86Immediat,)
    args_doc = ("nb of dword to clean",)
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Clean, cls).assemble(ass, args, no_regs)
        #Clening Arg need to preserv EAX
        no_regs.add(X86Register('ESP'))
        args = (args[0].value,)
        #Clean 0 : do nothing
        if args[0] == 0:
            return Clean(0, 0, None)
        best_cleaner = (0xffffffff, None)
        for cleaner in cls.gadgets[0] + cls.gadgets[1]:
            clean_size =  cls.calc_clean_size(cleaner, no_regs)
            if args[0]  <= clean_size < best_cleaner[0]:
                best_cleaner = (clean_size, cleaner)
                if clean_size == args[0]:
                    return Clean(args[0], clean_size, cleaner)
        if not best_cleaner[1]:
            return None
        return Clean(args[0], *best_cleaner)

    #This method take care of no_regs
    @classmethod
    def calc_clean_size(cls, cleaner, no_regs):
        #First instr just invalid ESP : We know this !
        if no_regs & cleaner.invalid_register(1):
            return -1
        consumption = cleaner.stack_consumption() - X86Register.DEFAULT_REG_SIZE
        if cleaner.instrs[-1].mnemo == 'retn':
            consumption -= cleaner.instrs[-1].args[0]
        if consumption % X86Register.DEFAULT_REG_SIZE:
            return -1
        return consumption / X86Register.DEFAULT_REG_SIZE

    def __init__(self, size_to_clean, clean_size, cleaner):
        super(Clean, self).__init__()
        if size_to_clean == 0:
            self.debug = "clean {0} DWORD on the stack".format(size_to_clean)
            return
        self.debug = "clean {0} DWORD on the stack : <{1}>".format(size_to_clean, cleaner.descr)
        self.stack.append(cleaner.vaddr)
        self.stack.add_garbage((clean_size - size_to_clean) * X86Register.DEFAULT_REG_SIZE)

    def __str__(self):
        return self.debug
