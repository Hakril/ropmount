#!/usr/bin/python2

from .call import Call
from ..ropinstr import RopInstr, AddrOrSym
from ropmount.archi.intel.x86 import X86Register, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException
from ropmount.mapfile.kernel import UnixKernel, WindowsKernel

class Leak(Call):
    """
        Leak value at addr and send the result on a socket
    """
    mnemo = "leak"
    templates = []
    args = (AddrOrSym,) + (X86Immediat, X86Immediat)
    args_doc = ("addr", "fd", "size")
    args_min = 2

    #Most to least efficient (in stack size)
    NULL = X86Immediat(0, X86Immediat.default_size)
    leak_functions = {
            UnixKernel : [
                ("write" , lambda addr, fd, size : (fd, addr, size)),
                ("send" , lambda addr, fd, size : (fd, addr, size, NULL))],
            WindowsKernel : [
                    ("send" , lambda addr, fd, size : (fd, addr, size, NULL))]
            }



    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        #Clening Arg need to preserv EAX
        if len(args) < 3:
            #If no size : leak default DWORD size
            args = args + (X86Immediat(X86Immediat.default_size, X86Immediat.default_size),)
        addr, fd, size = args
        for symbol, usage in cls.leak_functions[ass.kernel]:
            try:
                addr_leak_func = ass.symbols[symbol]
            except KeyError:
                continue
            leak_args = usage(addr, fd, size)
            if ass.kernel is UnixKernel:
                call = ass.raw_assemble('call', (addr_leak_func,) + leak_args, no_regs)
            elif ass.kernel is WindowsKernel:
                call = ass.raw_assemble('pecall', (X86Immediat(1, X86Immediat.default_size), addr_leak_func,) + leak_args, no_regs)
            else:
                raise ValueError("Unknow kernel : {0}".format(ass.kernel.__name__))

            return call
        return None


    def __init__(self, func_name, func_addr, args, cleaner):
        raise NotImplementedError("cannot init leak ropinstr : should use <call / pecall>")


    def __str__(self):
        return self.debug
