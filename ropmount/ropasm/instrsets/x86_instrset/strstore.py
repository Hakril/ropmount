#!/usr/bin/python2

from ..ropinstr import RopInstr, MultipleInstr, ImmedOrReg
from ropmount.archi.intel.x86 import X86Register, X86RegSet, X86Immediat

class StrStore(RopInstr):
    """
        Store a string in memory
        if register used as dest : register will point just after string's end
    """
    mnemo = "strstore"
    templates = []
    args = (ImmedOrReg, str)
    args_doc = ("Dest", "value")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(StrStore, cls).assemble(ass, args, no_regs)
        no_regs.add(X86Register("ESP"))
        if type(args[0]) == X86Immediat:
            return cls.assemble_to_int(ass, args, no_regs)
        return cls.find_good_reg(ass, args, no_regs)
    #return cls.assemble_to_reg(ass, args, no_regs)

    @classmethod
    def find_good_reg(cls, ass, args, no_regs):
        if not args[0] in no_regs:
            add = cls.test_assemble_reg(ass, args[0], no_regs)
            if add:
                return cls.assemble_to_reg(ass, args, no_regs)

        reg32 = set(map(X86Register, X86Register.valid32))
        reg32 -= no_regs | X86RegSet([X86Register("ESP")])
        res = {}
        for reg in reg32:
            if cls.test_assemble_reg(ass, reg, no_regs):
                premov = ass.raw_assemble('mov', (reg, args[0]), no_regs)
                postmov = ass.raw_assemble('mov', (args[0], reg), no_regs)
                if not premov or not postmov:
                    continue
                write = cls.assemble_to_reg(ass, (reg, args[1]), no_regs)
                multi = MultipleInstr()
                multi.merge(premov)
                multi.merge(write)
                multi.merge(postmov)
                res[len(multi)] = multi
        if res:
            return res[min(res)]

    @classmethod
    def test_assemble_reg(cls, ass, reg, no_regs):
        tmp = ass.raw_assemble('write', (reg, X86Immediat(0xDEADCAFE, X86Immediat.default_size)), no_regs)
        add = ass.raw_assemble('add', (reg, X86Immediat(4, X86Immediat.default_size)), no_regs)
        return tmp and add

    @classmethod
    def assemble_to_reg(cls, ass, args, no_regs):
        str_to_store = args[1] + "\x00"
        elts = StrStore.split_string(str_to_store, X86Register.DEFAULT_REG_SIZE)
        res = []
        for elt in elts:
            tmp = ass.raw_assemble('write', (args[0], X86Immediat(elt, X86Immediat.default_size)), no_regs)
            if tmp:
                add = ass.raw_assemble('add', (args[0], X86Immediat(4, X86Immediat.default_size)), no_regs)
            if not tmp or not add:
                raise ValueError("cant write str ")
            res.append(tmp)
            res.append(add)
        return StrStore(res, args)

    @classmethod
    def assemble_to_int(cls, ass, args, no_regs):
        str_to_store = args[1] + "\x00"
        elts = StrStore.split_string(str_to_store, X86Register.DEFAULT_REG_SIZE)
        res = []
        addr = args[0]
        for elt in elts:
            tmp = ass.raw_assemble('write', (addr, X86Immediat(elt, X86Immediat.default_size)), no_regs)
            addr.value += 4
            if not tmp:
                raise ValueError("cant write str ")
            res.append(tmp)
        return StrStore(res, args)

    @classmethod
    def split_string(cls, string, word_size):
        pad = (word_size - len(string) % word_size) % word_size
        string = string + 'P' * pad
        res = []
        for i in range(len(string) / word_size):
            res.append(int(string[i * word_size: (i + 1) * word_size][::-1].encode('hex'), 16))
        return res

    def __init__(self, writes, args):
        super(StrStore, self).__init__()
        self.debug = "strstore {0}, \"{1}\"".format(args[0], args[1])
        for write in writes:
            self.merge(write)
            self.debug = self.debug + "\n  " + str(write).replace("\n", "\n  ")

    def __str__(self):
        return self.debug
