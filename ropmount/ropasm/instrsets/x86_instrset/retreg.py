#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86 import X86Register

class RetReg(RopInstr):
    """
        Return on a register : may not preserv the register
    """
    mnemo = "retreg"
    templates = ['push REG32;ret', 'jmp REG32; {,}ANY;RET']
    args = (X86Register,)
    args_doc = ("Reg to ret on",)
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(RetReg, cls).assemble(ass, args, no_regs)
        res = {}
        for push_ret in cls.gadgets[0]:
            ret_reg = push_ret.instrs[0].args[0]
            if ret_reg in no_regs:
                continue
            #Direct retreg
            if ret_reg == args[0]:
                return RetReg(None, push_ret, args[0])
            mov = ass.raw_assemble('mov', (ret_reg, args[0]), no_regs)
            if mov:
                ret = RetReg(mov, push_ret, args[0])
                res[len(ret.stack)] = ret
        for jmp in cls.gadgets[1]:
            jmp_reg = jmp.instrs[0].args[0]
            #Direct retreg
            if jmp_reg == args[0]:
                return RetReg(None, jmp, args[0], 'jmp {0}'.format(jmp_reg))
            mov = ass.raw_assemble('mov', (jmp_reg, args[0]), no_regs)
            if mov:
                ret = RetReg(mov, jmp, args[0], 'jmp {0}'.format(jmp_reg))
                res[len(ret.stack)] = ret
        if res:
            return res[min(res)]

    def __init__(self, mov, gadget, ret_reg, descr=''):
        super(RetReg, self).__init__()
        self.debug = "Ret on Reg :{0}".format(ret_reg)
        if mov:
            self.merge(mov)
            self.debug += "\n  " + str(mov)
            self.debug += "\n  retreg {0}".format(gadget.instrs[0].args[0])
        self.stack.append(gadget.vaddr)
        if not descr:
            descr = gadget.descr
        self.debug += "  <{0}>".format(descr)

    def __str__(self):
        return self.debug
