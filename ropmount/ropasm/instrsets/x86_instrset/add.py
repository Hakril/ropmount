#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.archi.intel.x86 import X86Register, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException

class Add(RopInstr):
    """
        Add a value to a register
    """
    mnemo = "add"
    templates = ['add REG32, REG32;ret', 'add REG32, CONST;ret', 'inc REG32;ret']
    args = (X86Register, X86Immediat)
    args_doc = ("Dest", "Value to add")
    args_min = 2
    max_inc = 8

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Add, cls).assemble(ass, args, no_regs)
        if args[0] in no_regs:
            raise AssemblerException("add into forbiden register <{0}>".format(args[0]))
        if args[0] == X86Register('ESP'):
            raise AssemblerException('Can\'t add to ESP : use "clean" instead')
        res = {}
        simple_add = cls.get_direct_add(ass, args, no_regs)
        if simple_add:
            res[len(simple_add.stack)] = simple_add
        reg_add = cls.reg_add(ass, args, no_regs)
        if reg_add:
            res[len(reg_add.stack)] = reg_add
        if args[1].value <= Add.max_inc:
            inc_add = IncAdd.assemble(ass, args, no_regs)
            if inc_add:
                res[len(inc_add.stack)] = inc_add
        if res:
            return res[min(res)]
    @classmethod
    def get_direct_add(cls, ass, args, no_regs):
        simple_adds = ass.specialize(cls.gadgets[1], 'add REG32, {0};ret'.format(args[1]))
        res = {}
        for simple_add in simple_adds:
            add_reg = simple_add.instrs[0].args[0]
            if add_reg == args[0]:
                return Add(args, simple_add, [], [])
            if simple_add.invalid_register(0) & no_regs:
                continue
            premov = ass.raw_assemble('mov', (add_reg, args[0]), no_regs)
            postmov = ass.raw_assemble('mov', (args[0], add_reg), no_regs)

            if not (premov and postmov):
                continue
            add = Add(args, simple_add, [premov], [postmov])
            res[len(add.stack)] = add
        if res:
            return res[min(res)]

    @classmethod
    def reg_add(cls, ass, args, no_regs):
        reg_adds = ass.specialize(cls.gadgets[0], 'add REG32, REG32;ret'.format())
        res = {}
        for reg_add in reg_adds:
            src_reg = reg_add.instrs[0].args[1]
            dst_reg = reg_add.instrs[0].args[0]
            if src_reg == dst_reg:
                continue
            if dst_reg == args[0] and not src_reg in no_regs:
                pre_set = ass.raw_assemble('set', (src_reg, args[1]), no_regs)
                if pre_set:
                    add = Add(args, reg_add, [pre_set], [])
                    res[len(add.stack)] = add
                continue
            if src_reg == args[0] and not dst_reg in no_regs:
                pre_set = ass.raw_assemble('set', (dst_reg, args[1]), no_regs)
                post_mov = ass.raw_assemble('mov', (args[0], dst_reg), no_regs)
                if pre_set and post_mov:
                    add = Add(args, reg_add, [pre_set], [post_mov])
                    res[len(add.stack)] = add
                continue

            #dst and src or not args[0]
            #Try 1:                     #Try 2:
            #   mov src, args[0]        #   mov dst, args[0]
            #   set dst, args[1]        #   set src, args[1]
            #   add dst, src            #   add dst, src
            #   mov args[0], dst        #   mov args[0], dst
            post_mov = ass.raw_assemble('mov', (args[0], dst_reg), no_regs)
            if not post_mov or src_reg in no_regs or dst_reg in no_regs:
                continue
            #Try1
            pre_mov = ass.raw_assemble('mov', (src_reg, args[0]), no_regs)
            pre_set = ass.raw_assemble('set', (dst_reg, args[1]), no_regs)
            if pre_mov and pre_set:
                add = Add(args, reg_add, [pre_set, pre_mov], [post_mov])
                res[len(add.stack)] = add
            #Try2
            pre_mov = pre_set = None
            pre_mov = ass.raw_assemble('mov', (dst_reg, args[0]), no_regs)
            pre_set = ass.raw_assemble('set', (src_reg, args[1]), no_regs)
            if pre_mov and pre_set:
                add = Add(args, reg_add, [pre_set, pre_mov], [post_mov])
                res[len(add.stack)] = add
        if res:
            return res[min(res)]


    @classmethod
    def assemble_from_const(cls, ass, args, no_regs):
        #TODO : implem add from const
        # assemble 'add ecx, 5' using:
        # add ecx, 3 + add ecx, 2
        return None

    def __init__(self, args, add, preadd, postadd):
        super(Add, self).__init__()
        self.debug = "add {0}, {1} :".format(str(args[0]), args[1])
        for pre_instr in preadd:
            self.merge(pre_instr)
            self.debug += "\n  " + str(pre_instr) + "\n"
            self.debug += "  add {0}, {1} : ".format(*add.instrs[0].args)
        self.stack.append(add.vaddr)
        self.debug += "  <" + add.descr + ">\n"
        for post_instr in postadd:
            self.merge(post_instr)
            self.debug += "  " + str(post_instr).replace("\n", "\n  ") + "\n"

    def __str__(self):
        return self.debug


class IncAdd(Add):
    @classmethod
    def assemble(cls, ass, args, no_regs):
        res = {}
        for inc in Add.gadgets[2]:
            inc_reg = inc.instrs[0].args[0]
            if inc_reg == args[0]:
                return IncAdd(inc, None, args)
            if inc_reg in no_regs:
                continue
            premov = ass.raw_assemble('mov', (inc_reg, args[0]), no_regs)
            postmov = ass.raw_assemble('mov', (args[0], inc_reg), no_regs)
            if not postmov or not premov:
                continue
            incadd = IncAdd(inc, (premov, postmov, inc_reg), args)
            res[len(incadd.stack)] = incadd
        if res:
            return res[min(res)]

    def __init__(self, inc_gadget, mov_info, args):
        super(Add, self).__init__()
        self.debug = "Add {0}, {1} : \n".format(*args)
        inc_reg = args[0]
        if mov_info:
            premov, postmov, inc_reg = mov_info
            self.merge(premov)
            self.debug += "  {0}\n".format(str(premov).replace("\n", "\n  "))
        for _ in range(args[1].value):
            self.stack.append(inc_gadget.vaddr)
        self.debug += "  inc {0} * {1} times : <{2}>\n".format(inc_reg, args[1], inc_gadget.descr)
        if mov_info:
            self.merge(postmov)
            self.debug += "  {0}\n".format(str(postmov).replace("\n", "\n  "))

    def __str__(self):
        return self.debug
