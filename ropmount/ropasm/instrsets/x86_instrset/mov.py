#!/usr/bin/python2

from collections import deque
import copy

from ..ropinstr import RopInstr, MultipleInstr, BestLen, RopStack
from ropmount.archi.intel.x86 import X86Register, X86RegSet
from ropmount.ropasm.assemblerexception import AssemblerException



#This class is not a mov cause must not be call for submov
class MultipleMov(MultipleInstr):
    """
        A Class that Implement a merge to easier multiple mov search
        And respect Mov attributs
    """

    def __init__(self, src_reg, dst_reg):
        super(MultipleMov, self).__init__()
        self.debug_head = "mov {0}, {1}:".format(src_reg, dst_reg)
        self.src = src_reg
        self.dst = dst_reg

    def merge(self, other):
        self.mov_done = len(self.stack.values) + 1
        #dec for every effective merge of value (RopStack.next_addr)
        self.mov_done -= self.stack.values.count(RopStack.next_addr)
        super(MultipleMov, self).merge(other)

    def __copy__(self):
        """
            Dummy copy implem
        """
        m = MultipleMov(self.src, self.dst)
        m.debug = self.debug
        m.stack = RopStack()
        
        m.stack.values.extend(list(self.stack.values))
        if not self.debug_head is MultipleInstr.debug_head:
            m.debug_head = self.debug_head
        return m


class Mov(RopInstr):
    """
        Mov a register to another : it doesn't preserve the source register
    """
    mnemo = "mov"
    templates = ['mov REG32, REG32;{,}ROP;RET']
    args = (X86Register, X86Register)
    args_doc = ("Dest", "Src")
    args_min = 2
    rec = 0

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Mov, cls).assemble(ass, args, no_regs)
        #Empty mov is easy
        if args[1] == args[0]:
            return Mov(None)
        if args[0] in no_regs:
            raise AssemblerException("mov into forbiden register <{0}>".format(args[0]))
        mov = cls.get_simple_mov(ass, args, no_regs)
        if mov:
            return mov
        movs = Mov.assemble_complex(ass, args, no_regs)
        return movs

    @classmethod
    def get_submov(cls, ass, args, no_regs):
        """
            Search mov using Mov subclasses that don't have
            mov_nocall class attribute
        """
        res = {}
        for mov_subclass in Mov.__subclasses__():
            sub_mnemo = mov_subclass.mnemo
            sub_mov = ass.raw_assemble(sub_mnemo, args, X86RegSet(no_regs))
            if sub_mov:
                res[len(sub_mov.stack)] = sub_mov
        if res:
            return res[min(res)]

    @classmethod
    def save(cls, ass, mov, args, no_regs):
        """
            Save mov in assembler memoization process
        """
        ass.save(cls, args, no_regs, mov)

    @classmethod
    def assemble_complex(cls, ass, args, no_regs):
        """
            Search mov by merging mov of any sort
        """
        best = BestLen(None)
        m = MultipleMov(*args)
        #queue contain:
        #       args(dst, src) to compute
        #       current Multiple instr (previous find mov)
        #       Unusable register for mov (no_regs + already use)
        queue = deque([(args, m, no_regs | set([args[1]]))])
        #If we found a simple mov : register it
        simple_mov = cls.get_simple_mov(ass ,args, no_regs)
        best.value = simple_mov
        ##Register complexe mov if any
        sub_mov = cls.get_submov(ass, args, no_regs)
        best.value = sub_mov
        all_regs = set(map(X86Register, X86Register.valid32))
        while queue:
            args, cur_instr, unusable = queue.popleft()
            #If the stack of this gadget is more complexe that the best instr already found:
            # abort
            if best.value and len(cur_instr.stack) >= len(best.value.stack):
                continue
            #If we found a valid simple gadget : add it if it's the best
            simple_mov = cls.get_simple_mov(ass ,args, no_regs)
            if simple_mov:
                m = copy.copy(cur_instr)
                #m.mov_done = m.mov_done = len(m.stack.values) + 1
                m.merge(simple_mov)
                best.value = m
            sub_mov = cls.get_submov(ass, args, no_regs)
            if sub_mov:
                m = copy.copy(cur_instr)
                #m.mov_done = m.mov_done = len(m.stack.values) + 1
                m.merge(sub_mov)
                best.value = m
            #Add all complex_mov in queue
            for possible_reg in all_regs - unusable:
                tmp_best = BestLen(None)
                tmp_best.value = cls.get_simple_mov(ass, (possible_reg, args[1]), no_regs)
                tmp_best.value = cls.get_submov(ass, (possible_reg, args[1]), no_regs)
                if tmp_best.value:
                    m = copy.copy(cur_instr)
                    #m.mov_done = m.mov_done = len(m.stack.values) + 1
                    m.merge(tmp_best.value)
                    queue.append(((args[0], possible_reg), m, unusable | set([possible_reg])))
        return best.value

    @classmethod
    def get_simple_mov(cls, ass, args, no_regs):
        """
            Assemble a simple mov
            Use assembler memoization
        """
        found, res = ass.get(cls, args, no_regs)
        if found:
            return res
        best = BestLen(None)
        simple_mov = ass.specialize(cls.gadgets[0], 'mov {0}, {1};{{,}}ROP;RET'.format(args[0], args[1]))
        for mov in simple_mov:
            if cls.is_valid(mov, args[0], no_regs):
                best.value = Mov(mov)
        # Not found doesn't not mean there is no mov:
        # no saving if not found
        if best.value:
            cls.save(ass, best.value, args, no_regs)
            return best.value

    @classmethod
    def is_valid(cls, mov, dest, no_regs):
        """ Return true if gadget respect no_regs and dest is not invalidated """
        return not no_regs & mov.invalid_register(0) and dest not in mov.invalid_register(1)

    #instanciate a simple Mov
    def __init__(self, mov):
        super(Mov, self).__init__()
        self.debug = []
        if not mov:
            self.dst = self.src = "?"
            self.debug = ["Empty mov"]
            return

        self.src = mov.instrs[0].args[1]
        self.dst = mov.instrs[0].args[0]

        self.stack.append_gadget(mov, [])
        self.debug.append(mov.descr)
        #This is a simple mov : so the mov is effectiv juste after jumping on the gadget
        self.mov_done = 1


    def __str__(self):
        return "mov {0}, {1} : <{2}>".format(self.dst, self.src, "; ".join(self.debug))


