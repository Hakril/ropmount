#!/usr/bin/python2

from copy import copy
from ..ropinstr import RopInstr, AddrOrSym
from ropmount.archi.intel.x86 import X86Register, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException

class Call(RopInstr):
    """
        Call a function with given args
        Also clean the stack
    """
    mnemo = "call"
    templates = []
    args = (AddrOrSym,) * 7
    args_doc = ("function name",) + tuple("arg" + str(i) for i in range(6))
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        #Cleaning Arg need to preserv EAX
        nb_args = len(args) - 1
        addr = args[0]
        #No args
        if not nb_args:
            return Call(args[0], addr, [], None)
        #args : need a cleaner
        pop = cls.get_cleaner(ass, nb_args, no_regs)
        if not pop:
            return None
        return Call(args[0].name, addr, args[1:], pop)

    @classmethod
    def get_cleaner(cls, ass, nb_args, no_regs):
        no_regs = copy(no_regs)
        no_regs.extend([X86Register('EAX')])
        pop = ass.raw_assemble('clean', (X86Immediat(nb_args, X86Immediat.default_size),), no_regs)
        return pop

    def __init__(self, func_name, func_addr, args, cleaner):
        super(Call, self).__init__()
        self.name = func_name
        self.stack.append_function(func_addr, args)
        if cleaner:
            self.merge(cleaner)
        self.debug = "call {0} <{2}>\n{1}".format(func_name, str(cleaner), ",".join([arg.name for arg in args]))

    def __str__(self):
        return self.debug
