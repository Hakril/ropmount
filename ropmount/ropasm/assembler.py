#!/usr/bin/python2

import cPickle as pickle
import md5

import ropmount.ropfinding as RopFinding
import instrsets

from .instrsets.ropinstr import ImmedOrReg, MultipleInstr, AddrOrSym
from ropmount.ropfinding.filters.intfilter import IntFilter
from .assemblerexception import AssemblerException


class RopAssembler(object):
    """
        The class that parse an instruction and assemble it
        This is also the class that mess with Filters and RopFinding
            - Search Gadget for RopInstr
            - Specialize some RopInstr gadgets
    """
    def __getstate__(self):
        data = dict((instr, instr.gadgets) for instr in self.instrset.values())
        return (self.mapfile, self.finder, self.archi, data, self.mem)

    def __setstate__(self, state):
        self.mapfile = state[0]
        self.finder = state[1]
        self._init_symbols()
        self._init_kernel(self.mapfile.kernel)
        self._init_archi_params(state[2])
        self.instrset = {}
        for instr, gadgets in state[3].items():
            instr.gadgets = gadgets
            self.instrset[instr.mnemo] = instr
        self.mem = state[4]

    def _init_archi_params(self, archi):
        #instruction set is the instruction set of the given archi
        self.instrset = dict(((i.mnemo, i) for i in instrsets.all[archi.name].instrs))
        self.archi = archi
        self.archi_const_reg = ImmedOrReg(archi.Register, archi.Immediat)
        self.archi_addr_sym = AddrOrSym(archi.Immediat, self.symbols)

        self.args_parse = {
                        archi.Immediat : lambda x: archi.Immediat(x, archi.Immediat.default_size),
                        archi.Register : lambda x : archi.Register(x),
                        ImmedOrReg : lambda x: self.archi_const_reg.parse(x),
                        AddrOrSym : lambda x: self.archi_addr_sym.parse(x),
                        str : lambda x : x,
                      }

    def _init_kernel(self, kernel):
        self.kernel = kernel

    def _init_symbols(self):
        self.functions = self.mapfile.get_functions()
        self.symbols = self.mapfile.get_symbols()

    def _init_ropinstr_gadgets(self):
        all_templates = []
        # group together all filters of all instruction in the set
        for instr in self.instrset.values():
            self.mem[instr] = {}
            all_templates.extend((t for t in instr.templates))
        all_filters = [IntFilter(t, self.archi) for t in all_templates]
        self.res = dict(((name, i) for name,i in zip(all_templates, self.finder.find_gadget(all_filters))))
        # give to each instr the result of its templates
        for instr in self.instrset.values():
            instr.gadgets = [self.res[template] for template in instr.templates]

    def __init__(self, mapfile, finder=None):
        self.mapfile = mapfile
        if not finder:
           finder = RopFinding.RopFinder(mapfile)
        self._init_symbols()
        self.finder = finder
        self.mem = {}
        archi = mapfile.archi
        self.kernel = mapfile.kernel
        self._init_archi_params(archi)
        self._init_ropinstr_gadgets()

    def specialize(self, gadgets, spe_template):
        """ Permit to apply re-filtering of gadget during assembling
            Exemple:
                we have a list that match : "pop REG32; ret"
                and we want to pop EAX
                We can re-filter this list:
                specialize(list, "pop EAX;ret")
        """
        spe_filter = IntFilter(spe_template, self.archi)
        return [gadget for gadget in gadgets if spe_filter.match(gadget)]

    def assemble(self, ropprog, raise_failure=False):
        """
            Parse a string representing some instructions and send it to
            raw_assemble
        """
        instrs = ropprog.strip().split(';')
        res = []
        for instr in instrs:
            infos = instr.split('!')

            code = infos[0]
            no_regs = self.archi.RegSet()
            if len(infos) > 2:
                raise AssemblerException("More that one '!' in instr <{0}>".format(instr))
            if len(infos) == 2:
                for reg in infos[1].split(','):
                    no_regs.add(self.archi_const_reg.parse(reg.strip()))

            instr_info = code.strip().split(" ", 1)
            if instr_info[0] not in self.instrset:
                raise AssemblerException('Unknow instruction : {0}'.format(instr_info[0]))
            if len(instr_info) < 2:
                raise AssemblerException("instruction to assemble need at least one argument")
            mnemo, str_args = instr_info
            instr = self.instrset[mnemo]
            args = []
            #No list comp here to give the best message if any error
            for arg_type, arg_value in zip(instr.args, str_args.split(',')):
                try:
                    args.append(self.args_parse[arg_type](arg_value.strip()))
                except ValueError as e:
                    raise AssemblerException("Error when parsing argument {0} <{1}> : expecting type <{2}>\n"
                            "Info : {3}".format(len(args) + 1, arg_value, arg_type.__name__, e))

            i = self.raw_assemble(mnemo, tuple(args), no_regs)
            if not i:
                if raise_failure:
                    raise AssemblerException("Can't assemble : <{0}>".format(code))
                return None
            res.append(i)

#        if len(res) == 1:
#            return res[0]
        ret = MultipleInstr()
        for i in res:
            ret.merge(i)
        return ret

    def raw_assemble(self, mnemo, args, no_regs=None):
        """
            Assemble a single instr:
            Used by
                    - RopAssembler.assemble
                    - RopInstr
        """
        if no_regs is None:
            no_regs = self.archi.RegSet()
        if mnemo not in self.instrset:
            raise AssemblerException("Unknow instr : <{0}>".format(mnemo))

        instr = self.instrset[mnemo]
        for arg, expect_type in zip(args, instr.args):
            if not isinstance(arg, expect_type):
                raise AssemblerException("Bad type Instr {0} : {1} expected ".format(arg.__class__, expect_type))
        found, res = self.get(instr, args, no_regs)
        if not found:
            res = instr.assemble(self, args, self.archi.RegSet(no_regs)) #Deep copy of no_regs
            self.save(instr, args, no_regs, res)
        return res

    def get(self, instrcls, args, no_regs):
        """
            Restore already assembled instr depending on args and no_regs
            return tuple : (Found, Instr)
        """
        instrmem = self.mem[instrcls]
        if args not in instrmem:
            return (False, None)
        all_res = [(self.consum(i), i, noreg) for noreg,i in instrmem[args] if no_regs <= noreg]
        if not all_res:
            return (False, None)
        #If something is found:
        best = min(all_res)
        if  best[1]:
            return (True, min(all_res)[1])
        if not best[2] ==  no_regs:
            return (False, None)
        return (True,None)

    def save(self, instrcls, args, no_regs, result):
        """
            Save a assembled instruction with its args and no_regs
        """
        instrmem = self.mem[instrcls]
        if args not in instrmem:
            instrmem[args] = [(no_regs, result)]
            return
        instrmem[args] = [(noreg, ins) for noreg, ins in instrmem[args] if
                not noreg <= no_regs or self.consum(ins) < self.consum(result)]
        instrmem[args].append((no_regs, result))

    def consum(self, x):
        """
            return the stack consumption of x
            or a big value if None
        """
        if not x:
            return 0xffffffff
        return len(x.stack)
