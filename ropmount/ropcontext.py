#! /usr/bin/python2

import itertools
import glob
import cPickle as pickle

import ropmount.mapfile as MapFile
import ropmount.ropfinding as RopFinding
import ropmount.ropasm as RopAsm
from ropmount.ropfinding.filters.intfilter import IntFilter
from ropmount.utils.recoverable import Recoverable


def rop_from_files(filenames):
        """create a RopContext from [one/a list of] filename"""
        if isinstance(filenames, list):
            mapfile = MapFile.MultipleMapFile([MapFile.create_mapfile(fn) for fn in filenames])
        else:
            mapfile = MapFile.create_mapfile(filenames)
        return RopContext(mapfile)

def rop_from_raw(data, archi_name):
        """create a RopContext from raw data"""
        mapfile = MapFile.RawMapFile(data, archi_name)
        return RopContext(mapfile)

def disable_ropcontext_recover():
    RopContext.recover = False

class RopContext(object):
    """
        Contain a complete Context.
        This the class that frontends use
    """
    __metaclass__ = Recoverable
    extension = ".rdp"

    def __init__(self, mapfile, recover=True):
        self.mapfile = mapfile
        self.filename = mapfile.filename
        self.finder = RopFinding.RopFinder(self.mapfile)
        self.archi = self.mapfile.archi
        self.assembler = None
        self.find_mem = {}

    #ropfinding frontend
    def find(self, template):
        """
            Find a gadget based on Asm-Template syntax
        """
        try:
            return self.find_all(template)[0]
        except IndexError:
            return None

    def find_all(self, template):
        """
            Find all gadgets based on Asm-Template syntax
        """
        #Dummy memoization
        if template in self.find_mem:
            return self.find_mem[template]
        res = self.find_list([template])[0]
        self.find_mem[template] = res
        return res


    def find_list(self, templates):
        filters = []
        for i, t in enumerate(templates):
            filters.append(IntFilter(t, self.archi))
        return self.find_all_raw(filters)

    def find_all_raw(self, filters):
        return self.finder.find_gadget(filters)

    #ropasm frontend
    def assemble(self, instr):
        """
            Assemble an instruction
        """
        if not self.assembler:
            self.assembler = RopAsm.RopAssembler(self.mapfile, finder=self.finder)
        return self.assembler.assemble(instr)

    #mapfile frontend
    def get_functions(self):
        """
            Return functions found in mapfile
        """
        return self.mapfile.get_functions()

    def get_symbols(self):
        """
            Return symbols found in mapfile
        """
        return self.mapfile.get_symbols()

    #Serialization stuff
    @classmethod
    def _recover(cls, mapfile):
        """
            This function is called by Recoverable metaclass
            Allow RopContext to be recovered from pickle file at instantiation
        """
        filename = mapfile.filename
        rdps = glob.glob(filename + cls.extension + ".*")
        for rdp in rdps:
            f = open(rdp, 'rb')
            try:
                descr = pickle.load(f)
            except EOFError:
                f.close()
                continue
            if not isinstance(descr, basestring):
                f.close()
                continue
            if not descr == filename:
                f.close()
                continue
            try:
                context = pickle.load(f)
            except EOFError:
                f.close()
                continue
            context.save_file = rdp
            f.close()
            return context
        return None

    def __del__(self):
        """
            Serialize RopContext into a file
        """
        try:
            if not self.recover:
                return
        except AttributeError:
            pass

        if hasattr(self, "save_file"):
            f = open(self.save_file, "wb")
        else:
            rdps = glob.glob(self.mapfile.filename + self.extension + ".*")
            for i in itertools.count(0):
                save_file = self.mapfile.filename + self.extension + "." + str(i)
                if  save_file not in rdps:
                    f = open(save_file, 'wb')
                    break
        if not f:
            return
        pickle.dump(self.mapfile.filename, f)
        pickle.dump(self, f)
        f.close()
