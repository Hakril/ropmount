#! /usr/bin/python2
"""A RopAssembler front-end based on a simple shell"""
import readline
import struct
import traceback

import ropmount
from ropmount.mapfile.mapfile import RelocableDumpError
from ropmount.ropasm.assemblerexception import AssemblerException
from ropmount.archi.type import Register, Immediat
from ropmount.ropasm.instrsets.dumper import StackDumperException

def Entry(filename):
    """Entry point of this front-end"""
    print "Loading.."
    rpc = ropmount.rop_from_files(filename)
    if rpc.assembler is None:
        print ("First initialization of RopAssembler : this could take some time")
        try:
            rpc.assemble('')
        except AssemblerException:
            pass
    instr = None

    print "simple assembler shell"
    print "options are:"
    for i in Options:
        print "    {0} : {1}".format(i, Options[i].__doc__)
    while True:
        try:
            cmd = raw_input(">>> ")
        except EOFError:
            break
        try:
            if not cmd:
                continue
            if cmd == "quit" or cmd == "exit":
                break
            if cmd in Options:
                Options[cmd](instr, rpc.assembler)
                continue
            instr = rpc.assemble(cmd)
            if instr:
                print instr
            else:
                print "Can't assemble this"
        except AssemblerException as e:
            print traceback.format_exc()
            print "Error :",
            print str(e)
            continue
    print "Leaving"



def print_stack(ropinstr, assembler):
    """Print the stack of the last assembled gadget"""
    if not ropinstr:
        print "no stack to print"
        return
    garbage_size = 0
    for immed in ropinstr.stack.values:
        if immed.size == 1:
            garbage_size += 1
            continue
        if garbage_size:
            print "{0} bytes of garbage".format(hex(garbage_size))
            garbage_size = 0
        print immed
    if garbage_size:
        print "{0} bytes of garbage".format(hex(garbage_size))

def dump_stack(ropinstr, assembler):
    """Dump last assembled gadget in a file"""
    if not ropinstr:
        print "no stack to dump"
        return
    dump_format = raw_input("dump format : default is 'raw' >")
    filename = raw_input("> File to dump stack into ?: ")
    if not dump_format:
        dump_format = "raw"
    try:
        res = ropinstr.stack.dump(dump_format)
    except (StackDumperException, RelocableDumpError) as e:
        print (e)
        if isinstance(e, RelocableDumpError):
            print ("You can use 'fix_addr' to fix the base address of the file")
        return
    try:
        f = open(filename, "wb")
        f.write(res)
        f.close()
    except IOError as e:
        print(e)
        return

def print_help(ropinstr, assembler):
    """Give some Help"""
    print ("This shell allow you to assemble ROP Stack for the file" +
            "'{0}' of archi : <{1}>".format(assembler.mapfile.filename, assembler.archi.name))
    print "To assemble an instruction use the following syntax :"
    print '"mnemo arg0,arg1,..!noreg1,noreg2,.."'
    print ("To list the instructions for archi <{0}> of command 'instrset'"
            .format(assembler.archi.name))

def print_instrset(ropinstr, assembler):
    """Give information about the available instruction set"""
    for mnemo, instr in assembler.instrset.items():
        print ""
        print '{0} :\n {1}'.format(mnemo, instr.__doc__)
        for atype, adoc in zip(instr.args, instr.args_doc):
            print "   '{1}' => {0}".format(normalize_atype(atype).__name__, adoc)

def normalize_atype(atype):
    """Return the original type of a RopInstr argument"""
    normalized = [Register, Immediat]
    for norm in normalized:
        if issubclass(atype, norm):
            return norm
    return atype

def print_functions(ropinstr, assembler):
    """ Print all function available (symbols in exec memory)"""
    print assembler.functions.keys()

def print_symbols(ropinstr, assembler):
    """ Print all symbols available"""
    print assembler.symbols.keys()

def launch_pdb(ropinstr, assembler):
    """Debug : Launch python debugger"""
    import pdb
    pdb.set_trace()

def fix_base(ropinstr, assembler):
    """Fix base address of a relocable file"""
    name = raw_input("file name? >")
    addr = int(raw_input("base addr ? >"), 0)
    try:
        lib = assembler.mapfile[name]
    except KeyError:
        print "Unknow lib <{0}>".format(name)
        return None
    lib.fix_baseaddr(addr)

Options = {
        'print' : print_stack,
        'dump' : dump_stack,
        'pdb' : launch_pdb,
        'help' : print_help,
        'instrset' : print_instrset,
        'fix_base' : fix_base,
        'functions' : print_functions,
        'symbols' : print_symbols,
        }
