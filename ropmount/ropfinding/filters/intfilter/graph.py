#!/usr/bin/python2


class Graph(object):
    """
        The graph that represente a gadget with {min, max}
        A graph was cleary not a good idea but I don't really what work one this

        If anyone what to work on this : it would be very appreciated
    """
    #Use Wrap cause we need to insert the same Instr as multiple entry in the Graph
    class Wrap(object):
        i = 0
        def __init__(self, value):
            self.value = value
            self.nb = self.i
            Graph.Wrap.i += 1

        def __str__(self):
            return "I{0}({1})".format(self.nb, self.value.mnemo.strip())


    def __init__(self):
        self.entries = []
        self.graph = {}

    def get_entries(self):
        return self.entries

    def add_instr(self, (meta_info, instr)):
        if not meta_info:
            self.add_mandatory_instr(instr)
        else:
            self.complex_add_instr(meta_info, instr)

    def add_parallel_instr(self, instrs):
        wraps = [Graph.Wrap(instr) for instr in instrs]
        for wrap in wraps:
            self.graph.update({wrap : self.entries})
        self.entries = wraps

    def add_mandatory_instr(self, instr):
        wrap = Graph.Wrap(instr)
        self.graph.update({wrap : self.entries})
        self.entries = [wrap]

    def add_optional_instr(self, instr):
        wrap = Graph.Wrap(instr)
        self.graph.update({wrap : self.entries})
        self.entries = [wrap] + self.entries

    def complex_add_instr(self, (min_nb, max_nb), instr):
        diff = max_nb - min_nb
        #mandatory instr
        for _ in range(min_nb):
            self.add_mandatory_instr(instr)
        #optional instr
        for _ in range(diff):
            self.add_optional_instr(instr)
        #{,} => 0 to inf
        if not min_nb and not max_nb:
            self.add_optional_instr(instr)
        #max == 0 => infini
        if not max_nb:
            self.graph[self.entries[0]].append(self.entries[0])

    def dump(self, filename):
        """
            [DEBUG] Generate a dot file of the Graph
        """
        f = open(filename, 'w+')
        f.write("digraph G {")
        f.write("entry;")
        for entry in self.entries:
            f.write('entry -> "{0}";'.format(entry))

        for wrap in self.graph:
            if not self.graph[wrap]:
                f.write('"{0}" -> out;'.format(wrap))
            for next in self.graph[wrap]:
                f.write('"{0}" -> "{1}";'.format(wrap, next))

        f.write("}")

    def match(self, list_instr):
        return self.rec_match((list_instr, 0), self.entries)

    def rec_match(self, (instr_list, pos), entries):
        if not entries and pos == len(instr_list):
            return True

        if len(instr_list) == pos:
            return False

        instr = instr_list[pos]
        for entry in entries:
            if (entry.value.match(instr) and
                self.rec_match((instr_list, pos +1), self.graph[entry])):
                return True
        return False
