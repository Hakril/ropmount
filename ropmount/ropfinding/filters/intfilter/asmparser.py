#!/usr/bin/python2

import re
from .graph import Graph
from ropmount.archi.type import Register, MemAccess, Immediat

#This is the code that parse the Generic-ASM given to the IntFilter
#Return a 'Matcher' : An object that verify the match of a Gadget

def parse(template, archi):

    class Matcher(object):
        @staticmethod
        def parse(string):
            raise NotImplementedError()

        def match(self, item):
            raise NotImplementedError()


    class ConstMatcher(Matcher):
        """
            Match an Immediat
        """
        @staticmethod
        def parse(string):
            if string.strip().upper() == 'CONST':
                return AnyConstMatcher()
            try:
                x = int(string, 0)
                return ConstMatcher(x)
            except ValueError:
                return None

        def match(self, immed):
            if not isinstance(immed, archi.Immediat):
                return False
            return self.value == immed.value

        def __init__(self, value):
            self.value = value

        def __repr__(self):
            return "{0}({1})".format(self.__class__.__name__, self.value)


    class AnyConstMatcher(ConstMatcher):
        """
            Match any Immediat
        """
        def __init__(self):
            self.value = 'ANY'

        def match(self, value):
            return isinstance(value, archi.Immediat)


    class RegMatcher(Matcher):
        """
            Match a register
        """
        @staticmethod
        def parse(string):
            string = string.strip().upper()
            if  string in archi.reg_group:
                return RegGroupMatcher(archi.reg_group[string])
            if string in archi.Register.valid_regs:
                return RegMatcher(archi.Register(string))
            return None

        def __init__(self, reg):
            self.reg = reg

        def match(self, reg):
            if not isinstance(reg, Register):
                return False
            return self.reg == reg

        def __repr__(self):
            return "{0}({1})".format(self.__class__.__name__, self.reg)

    class RegGroupMatcher(RegMatcher):
        """
            Match any REG32 Register
        """
        def __init__(self, reglist):
            self.reglist = reglist

        def match(self, reg):
            if not isinstance(reg, Register):
                return False
            return reg in self.reglist

        def __repr__(self):
            return "{0}(ANY)".format(self.__class__.__name__)

    class MemAccessMatcher(Matcher):
        """
            Match a Memory access of type [base_reg + index_reg * scale + Immed]
        """

        valid_scale = [ConstMatcher.parse(i) for i in ['1', '2','4','8', 'CONST']]

        @staticmethod
        def parse(string):
            string = string.strip()
            if len(string) < 2 or string[0] is not "[" or string[-1] is not "]":
                return None
            string = string[1:-1]
            if string == 'ANY':
                return AnyMemAccessMatcher()
            return MemAccessMatcher.parse_mem_formula(string)

        def __repr__(self):
            attrs = ["{0}={1}".format(name, getattr(self, name)) for
                    name in ["base", "index", "scale", "displacement"] if hasattr(self, name)]
            return "{0}({1})".format(self.__class__.__name__, ", ".join(attrs))

        #we can't have IndexRegister*Scale without BaseRegister
        @staticmethod
        def parse_mem_formula(string):
            atoms = [atom.strip() for atom in string.split('+')]
            if not 0 < len(atoms) <= 3:
                return None
            first_is_reg = RegMatcher.parse(atoms[0])
            if not first_is_reg:
                first_is_const = ConstMatcher.parse(atoms[0])
                if not first_is_const or len(atoms) > 1:
                    slide = MemAccessMatcher.parse_mul(atoms[0])
                    if not slide:
                        return None
                    return MemAccessMatcher(slide=slide)
                return MemAccessMatcher(displacement=first_is_const)
            if len(atoms) == 1:
                return MemAccessMatcher(base=first_is_reg)
            slide = MemAccessMatcher.parse_mul(atoms[1])
            if not slide:
                displace = ConstMatcher.parse(atoms[1])
                if not displace:
                    return None
                return MemAccessMatcher(base=first_is_reg, displacement=displace)
            if len(atoms) == 2:
                return MemAccessMatcher(base=first_is_reg, slide=slide)
            displacement = ConstMatcher.parse(atoms[2])
            if not displacement:
                return None
            return MemAccessMatcher(base=first_is_reg, slide=slide, displacement=displacement)

        @staticmethod
        def parse_mul(string):
            atoms = [atom.strip() for atom in string.split('*')]
            if len(atoms) is not 2:
                return None
            index =  RegMatcher.parse(atoms[0])
            scale = ConstMatcher.parse(atoms[1])
            if not index or not scale:
                return None
            if not any([ok_scale.value == scale.value for ok_scale in MemAccessMatcher.valid_scale]):
                return None
            return (index, scale)

        def __init__(self, base=None, slide=(None, None), displacement=None):
            self.base = base
            index, scale = slide
            self.index = index
            self.scale = scale
            self.displacement = displacement

#TODO : clean
        def match(self, memory):
            if not isinstance(memory, MemAccess):
                return False
            if bool(self.base) != bool(memory.base):
                return False
            if bool(self.index) != bool(memory.index):
                return False
            if bool(self.displacement) != bool(memory.displacement.value):
                return False
            if self.base and not self.base.match(memory.base):
                return False
            if self.index and (not self.index.match(memory.index) or not self.scale.match(memory.scale)):
                return False
            if self.displacement and not self.displacement.match(memory.displacement):
                return False
            return True

    class AnyMemAccessMatcher(MemAccessMatcher):
        """
            Match Any MemAccess
        """
        def __init__(self):
            pass

        def match(self, memorytype):
            return isinstance(memorytype, MemAccess)

        def __repr__(self):
            return "{0}(ANY)".format(self.__class__.__name__)

    class InstrMatcher(Matcher):
        """
            Match an instruction based on:
                - its mnemonic
                - its args
        """
        arg_parser = {
                        Immediat : ConstMatcher,
                        Register: RegMatcher,
                        MemAccess: MemAccessMatcher
                     }

        @staticmethod
        def parse(string):
            string = string.strip()
            parse_tuple = string.split(" ",1)
            mnemo = parse_tuple[0]
            args_join = None
            if len(parse_tuple) > 1:
                args_join = parse_tuple[1]
            if mnemo == "ANY":
                return AnyInstr()
            if mnemo == "ROP":
                return RopInstr()
            if mnemo not in archi.filter_instrs:
                raise ValueError("Unknow instr : {0}".format(string))
            if len(archi.filter_instrs[mnemo][0]) == 0:
                return InstrMatcher(mnemo)
            args = []
            if args_join:
                args = map(str.strip, args_join.split(","))
            if len(args) != len(archi.filter_instrs[mnemo][0]):
                raise ValueError("Bad bad arg number for instr {0} expect : {1}"
                                    .format(mnemo, len(archi.filter_instrs[mnemo][0])))
            for arg_types in archi.filter_instrs[mnemo]:
                parsed_args = [RopInstr.arg_parser[asmtype].parse(arg) for asmtype, arg in zip(arg_types, args)]
                if all(parsed_args):
                    return InstrMatcher(mnemo, parsed_args)
            raise ValueError("Bad instr : {0}".format(string))

        def __init__(self, mnemo, args=()):
            self.mnemo = mnemo
            self.args = args

        def match(self, instr):
            if not self.mnemo == instr.mnemo:
                    return False
            res = all([arg.match(argtype) for arg, argtype in zip(self.args, instr.args)])
            return res


    class AnyInstr(InstrMatcher):
        """
            Match any instruction
        """
        def __init__(self):
            pass

        def match(self, instr):
            return True

    class RopInstr(InstrMatcher):
        """
            Match valid ROP instruction
        """
        def __init__(self):
            pass

        def match(self, instr):
            return instr.is_valid_rop()

    class GadgetMatcher(Matcher):
        """
            Match a complete gadget : multiple instr with some repetition pattern
        """
        metainstr_re = re.compile("\s*(?P<meta_args>{[^}]*})?\s*(?P<instr>.*)")
        simple_metaarg_re = re.compile("{\s*(?P<value>\w*)\s*}")
        metaarg_re = re.compile("{\s*(?P<min>\w*)\s*,\s*(?P<max>\w*)\s*}")
        alias = {'RET' : ['ret', 'retn CONST']}
        def __init__(self, meta_instrs):
            self.graph = Graph()
            self.size = len(meta_instrs)
            for meta_info, str_instr in reversed(meta_instrs):
                str_instr = str_instr.strip()
                if str_instr in self.alias:
                    if meta_info:
                        #'{,} ALIAS' is not Implemented yet
                        raise NotImplementedError("{min,max} on Alias not implemented yet")
                    instrs = [InstrMatcher.parse(i) for i in self.alias[str_instr]]
                    self.graph.add_parallel_instr(instrs)
                else:
                    self.graph.add_instr((meta_info, InstrMatcher.parse(str_instr)))

        @staticmethod
        def parse(string):
            str_meta_instrs = string.split(';')

            if not str_meta_instrs[-1].strip()[0:3].lower() == 'ret':
                raise ValueError("Invalid template : gadget must end by (ret/retn/RET)")

            #get (meta_info, <Instr>) for each instr of str
            meta_instrs = ([GadgetMatcher.extract_info(str_meta_instr) for
                str_meta_instr in str_meta_instrs if str_meta_instr.strip()])
            return GadgetMatcher(meta_instrs)

        @staticmethod
        def extract_info(meta_instr):
            instr_match = GadgetMatcher.metainstr_re.match(meta_instr)
            if not instr_match:
                raise ValueError("WTF NO INSTR")
            meta_args = instr_match.group('meta_args')
            if not meta_args:
                return (None, instr_match.group('instr'))
            min_rep = 0
            max_rep = 0
            #Parse simple {value} meta_arg
            simple_args_match = GadgetMatcher.simple_metaarg_re.match(meta_args)
            if simple_args_match and simple_args_match.group('value'):
                try:
                    value_rep = int(simple_args_match.group('value'), 0)
                except ValueError:
                    raise ValueError("Bad value <{0}> for meta arg instr : int expected".format(simple_args_match.group('value')))
                return ((value_rep, value_rep), instr_match.group('instr'))
            #Parse  {min, max} meta_arg
            args_match = GadgetMatcher.metaarg_re.match(meta_args)
            if not args_match:
                raise ValueError("bad instruction meta_args : <{0}>".format(meta_args))
            if args_match and args_match.group('min'):
                try:
                    min_rep = int(args_match.group('min'), 0)
                except ValueError:
                    raise ValueError("Bad 'min' value {0} : int expected".format(args_match.group('min')))
            if args_match and args_match.group('max'):
                try:
                    max_rep = int(args_match.group('max'), 0)
                except ValueError:
                    raise ValueError("Bad 'max' value {0} : int expected".format(args_match.group('max')))
            if max_rep and min_rep > max_rep:
                ValueError("meta_info :  min > max")
            return ((min_rep,max_rep), instr_match.group('instr'))

    if template is not None:
        return GadgetMatcher.parse(template)
    #Debug purpose
    classes = ['ConstMatcher', 'MemAccessMatcher', 'RegMatcher', 'InstrMatcher']
    local = locals()
    return dict((name, local[name]) for name in classes)
