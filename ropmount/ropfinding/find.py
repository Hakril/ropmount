#! /usr/bin/python2

from ropmount.ropfinding.opcodefinder import OpcodeFinder
from ropmount.ropfinding.filters.ropfilter import RopFilter

class RopFinder(object):
    """
        The class that apply filters on a mapfile
    """

    def __init__(self, mapfile):
        self.mapfile = mapfile
        self.opfinder = OpcodeFinder(mapfile)

    def find_gadget(self, filters):
        """
            Returns a list of dict with filtered gadgets in form :
                {name : Gadget}
        """
        self.opfinder.rop_opcodes = []
        for f in filters:
            self.opfinder.rop_opcodes += f.interest_op
            f.clear_result()
        #Opcode finder will stop on every opcode that interest each filter
        self.opfinder.rop_opcodes = list(set(self.opfinder.rop_opcodes))
        for cur_opcode, validator in self.opfinder:
            # For each interesting opcode we search which filters are interested
            for f in filters:
                if cur_opcode in f.interest_op:
                    f.filter(validator)
        result = []
        for f in filters:
            filtered = f.get_filtered()
            result.append(filtered)
        return result
