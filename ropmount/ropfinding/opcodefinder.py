#!/usr/bin/python2

from ropmount.archi.gadget import GadgetDisas
import ropmount.archi as Disas

class ValidatorException(Exception):
    pass

class GadgetValidator(object):
    """
        This is the object that search valid gadgets
        backward from a begin instruction
    """
    MAX_FAIL = 8
    lol = 0

    def __init__(self, block_DB, base_vaddr):
        self.block_DB = block_DB
        self.base_vaddr = base_vaddr
        self.actual_pos = 0
        self.mem = {}
        last_instr = block_DB.disas(base_vaddr)
        if not last_instr:
           raise ValidatorException("can't assemble last instr")
        self.mem[0] = GadgetDisas([last_instr])

    def __iter__(self):
        """
            Return valid GadGetDisas with last instruction at index base_vaddr
        """
        index = 0
        fails = 0
        min_index = self.block_DB.vaddr - self.base_vaddr
        while fails != self.MAX_FAIL and index >= min_index:
            if index in self.mem:
                gadget =  self.mem[index]
            else:
                gadget = self.calc_GadgetDisas(index)
                self.mem[index] = gadget
            fails += 1
            if gadget:
                fails = 0
                self.actual_pos = -index
                yield gadget
            index -= 1

    def calc_GadgetDisas(self, index):
        """
           Disas instruction at index
           Check that the instr + others lead to the last instruction
        """
        instrdisas = self.block_DB.disas(self.base_vaddr + index)
        if not instrdisas or instrdisas.is_end_rop():
            return None
        index += instrdisas.size
        next_gadget = self.mem.get(index, None)
        #No gadget or gadget overflow on last_instr
        if index > 0 or not next_gadget:
            return None
        return GadgetDisas([instrdisas] + next_gadget.instrs)


class OpcodeFinder(object):
    """
        The class that search opcode from a mapfile:
        Will stop on every opcode in self.rop_opcodes list
    """

    def __init__(self, mapfile):
        self.mapfile = mapfile
        self.archi = mapfile.archi
        #The opcodes we will stop on
        self.rop_opcodes  = []

    def __iter__(self):
        """ Iter over all rop_opcodes on all mapfile segments
            return (opcode, validator)
        """
        for mapping in self.code_block_iterator():
            # We use ONE DisasDB per block to gain time
            # Create a DisasDB could take time (lot of call if one per validator)
            # Validator may redisas same instr that older validator : memoization
            block_DB = self.archi.DisasDB(mapping)
            for opcode, index in self.opcode_iterator(mapping):
                try:
                    yield (opcode, GadgetValidator(block_DB, index + mapping.vaddr))
                except ValidatorException as e:
                    print e
                    continue

    def code_block_iterator(self):
        """
            Iter over mapfile segments
            Return mapping
        """
        for mapping in filter(lambda mapp : 'X' in mapp.access, self.mapfile.get_mapping()):
            yield mapping

    def opcode_iterator(self, mapping):
        """
            Iter over all opcodes on a given segment
            return (opcode, index_in_segment)
        """
        block = mapping.get_code()
        for opcode in self.rop_opcodes:
            last_index = -1
            while True:
                next_index = block.find(opcode, last_index + 1)
                if next_index == -1:
                    break
                last_index = next_index
                yield (opcode, next_index)
