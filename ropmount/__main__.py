#!/usr/bin/python2

import argparse
from ropdump import  Entry as EntryDump
from asm import  Entry as EntryAss
from ropmount.ropcontext import disable_ropcontext_recover
import cProfile

def main():
    """ The entry point of ropmount
        Command line parser that call the good front-end
    """
    parser = argparse.ArgumentParser(description='ropmount launch options', prog='ropmount')
    parser.add_argument('--dump', action='append', metavar=['template'], help='Default option : dump all gadgets matching templates')
    parser.add_argument('--ass',  action='store_const', dest='Entry', const=EntryAss,   help='Launch RopAsm assembler shell')
    #Debug / Test option
    parser.add_argument('-n', action='store_true', dest='no_recover', help="Doesn't use file to recover/save data")
    parser.add_argument('--prof', action='store_true', dest='profiling', help='[DEBUG] launch with profiler')
    parser.add_argument('filename', nargs='+', action='store', default='', help='Executable file to analyse')

    #Need to find a cleaner way to check args
    args = parser.parse_args()


    if args.dump:
        if args.Entry != None:
            print ("Can't specify --ass and --dump together")
            parser.print_help()
            exit(1)
        args.Entry = EntryDump

    entries_args = {
            EntryAss : lambda args : (args.filename,),
            EntryDump : lambda args : (args.filename, args.dump),
            }

    if args.Entry is None:
        parser.print_help()
        exit(1)

    #Do not recover / save
    if args.no_recover:
        disable_ropcontext_recover()

    if args.profiling:
        cProfile.run('args.Entry(*entries_args[args.Entry](args))')
    else:
        args.Entry(*entries_args[args.Entry](args))

if __name__ == "__main__":
    main()
