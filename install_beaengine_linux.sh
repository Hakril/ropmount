# This is a `quickly done`script to compile bea-engine on linux
# I am pretty sure it wont work on every linux, but you can always use th zip file 'hakril.net/BeaEnginePythonLinuxFull.zip'
# It contains a modified version of BeaEngine (just 1 option + fix a comment) that should easily compile


wget http://hakril.net/BeaEnginePythonLinuxFull.zip
unzip  BeaEnginePythonLinuxFull.zip
cd BeaEnginePythonLinuxFull
ROOT_DIR=`pwd`
cd bea_sources
cmake CMakeLists.txt
make
GENERATED_OBJ=`find . -name "BeaEngine.c.o"`
DEST_DIR=`dirname $GENERATED_OBJ`
cd $DEST_DIR 
gcc --shared BeaEngine.c.o -o libBeaEngine.so
echo ==COMPILATION DONE==
echo You need to do following commands as root:
echo cp $ROOT_DIR/BeaEnginePython.py /usr/local/lib/python2.7/dist-packages
echo cp $ROOT_DIR/bea_sources/$DEST_DIR/libBeaEngine.so /usr/lib

