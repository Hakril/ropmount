#Using RopMount as a module

During CTF (or other) using ropmount as a module could be really helpful
to deal with special formatting and sending created ropstack.

To use RopMount as a module all you have to do is : "import ropmount"


##Begin roping

the ropmount module offer 2 front-end methods to create a RopContext :

    - `ropmount.rop_from_files` that takes a list of filename in argument
    - `ropmount.rop_from_raw` that takes raw data into a string and the architecture.
    ("INTEL32" is the only architecture for the moment)

These two methods return a RopContext object that can be used.

##Dealing with RopContext

In the following examples the RopContext object will be call `rpc`

### Finding gadget

To find gadget the RopContext offers 2 methods:

    - `rpc.find` that take an 'asm template' string and return a matching gadget if any
    - `rpc.find_all` that take an 'asm template' string and return a list of matching gadgets

(for more informations about the `asm template` read : `doc/ropdump/`)
Both methods return one (or a list of) Gadget objects (see section "Gadget objects")

### assembling ropstack

- `rpc.assemble` takes the ropstack to assemble as a string.
(for more informations about the `rop assembly` read `doc/ropasm/`)
This method return an Instr or None if assembling failed (see section "Instr and RopStack")

### Finding symbols and functions

RopContext also offers a way to get adresse of symbols from files.
those methods are:
    - rpc.get_symbols() : returns all symbols available.
    - rpc.get_functions() : returns all symbols in executable segments.

If you are working on multiple files : you can get the symbols from one file by using:
    - rpc.mapfile[FILENAME].get_symbols()(also works for the functions)

Symbols are `Immediat` object (see: "Instr, RopStack and Immediat")
NOTE ABOUT ELF:
        - the `read` symbol is the read address in the PLT and can be called and used to rop.
        - the  `read.got` symbol is the address for the `read` into the GOT and can be used to leak LIBC adress.

##Gadget objects

Gadget objects are returned by RopContext.find* methods.
Here is the list of the useful attributs of such objects:

    - gad.vaddr : the virtual address of the gadget : will be an offset if source file is a relocable object (see "Dealing with ASLR")
        The type returned is an `Immediat`
    - gad.descr : a string that represent the code of the gadget.
    - gad.instrs : A list of all instructions in the gadget (Architecture dependant)
    - gad.stack_consumption() : a method that return the number of bytes on the stack used by the gadget

Example:

    :::python
    >>> gad.descr
    'pop ebx;pop ebp;ret'
    >>> gad.vaddr
    X86Immediat(134516115,4)
    >>> gad.stack_consumption()
    12

##Instr, RopStack and Immediat

### Instr

Instr objects are returned by `RopContext.assemble` method.
This kind of object have 2 properties:

    - str(instr) return a human readable version of the instruction (the actual rop).
    - instr.stack return the stack needed to perfom the instruction represented by a RopStack object.

### Immediat

Immediat are a type (used in Gadget.vaddr) that contain the value and size (in bytes) of the Immediat.
To get the real value of an Immediat you should use : immed.dump().

WARNING : To dump a relocable immediat (as a gadget in a dynamic lib) you must fix the lib. baseaddr (see "Dealing with ASLR")

If you need the non-absolute address of a relocable immediat you can use: `imm.value`.
This value can't be use in a RopChain but can be useful to compute address in a dynamic lib.

### RopStack

Ropstack objects are the representation of the stack that will perform the rop.
RopStack have only one useful method for you:
    - stack.dump(format='')


This method dump the RopStack into a usable format.
Some format are already existing:
    - 'raw' : dump the stack in raw little-endian bytes
    - 'python' : dump the stack into python code that assemble the stack using struct

If you need you own format : you can call `stack.dump` without arguments:  
It will return a list of tuple of form (value, size) that you can use as you wish.

Example:

    :::python
    >>> rpc = ropmount.rop_from_raw("\x58\xC3", "INTEL32")
    ("\x58\xC3") is "pop eax;ret"

    >>> instr = rpc.assemble("set eax,0x42434445")
    >>> print(str(instr))
    Multiple 0x8:
      set EAX, 0x42434445 : <pop eax;ret>

    >>> instr.stack.dump()
    [(0, 4), (1111704645, 4)]
    (0 because gadget is at the beginning of our raw_data)
    (size are four because we are in IntelX86)

    >>> hex(1111704645)
    '0x42434445'

    >> instr.stack.dump('raw')
    '\x00\x00\x00\x00EDCB'

    >>> print(instr.stack.dump('python'))
    import struct
    res = ""
    res += struct.pack("<I", 0)
    res += struct.pack("<I", 1111704645)

## Dealing with ASLR and relocable files

When you work with relocable files (as dynamic libs) ropmount cannot know the base
adresse of those libs.
However ropmount is able to find gadget and assemble RopStack without knowing the exact adresse of the lib.
But RopMount need this base adresse to dump them for the final use.
To set the base adresse of a relocable file, you can use the following command:

    - rpc.mapfile[FILENAME].fix_baseaddr(base)

Example:

    :::python
    >>> rpc = ropmount.rop_from_files(["libc.so.6"])
    >>> x = rpc.assemble("set eax, 0")
    >>> x.stack.values
    [X86RelocableImmediat(158095,4), X86Immediat(0,4)]
    >>> x.stack.dump()
    Traceback (most recent call last):
    ...
    ValueError: Trying to dump a non fixed relocable mapping <libc.so.6>
    >>> rpc.mapfile["libc.so.6"].fix_baseaddr(10000000)
    >>> x.stack.dump()
    [(10158095, 4), (0, 4)]


## Full Example

A full example is available into `doc/example/module.py`
