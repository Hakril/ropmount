BeaEnginePython : ropmount segfault
===================================

If ropmount segfaults, it may be because you are using a new version of BeaEngine.
Sadly BeaEnginePython official repository have not been updated.
You can try to replace the file BeaEnginePython.py in your python lib by the one available here.
