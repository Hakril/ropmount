import sys
try:
    from setuptools import setup, find_packages
except ImportError:
    import distribute_setup
    distribute_setup.use_setuptools()
    from setuptools import setup, find_packages

if sys.version_info[0] != 2:
    print ("RopMount isn't working with python3 yet :(")
    sys.exit(msg)

setup(
    # General information.
    name='ropmount',
    description='A Roping Assistant',
    url='https://bitbucket.org/Hakril/ropmount',

    # Author information.
    author='Clement Rouault',
    author_email='clement.rouault@gmail.com',

    # File information.
    install_requires=open('requirements.txt').readlines(),
    packages=find_packages(exclude=['test', 'tests', 'examples', 'doc']),
    entry_points = {'console_scripts': ['ropmount = ropmount.__main__:main']},

    # PyPI categories.
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python',
    ],
)
