#!/usr/bin/python2

import ropmount.ropfinding.filters.intfilter.asmparser as asmparser
from ropmount.archi import archis
from tests.utils import *
from nose.tools import *


class AsmTest(ClassicTest):
    archi = None
    consts = {}
    regs = {}
    memaccess = {}
    instrs = {}

    def __init__(self):
        #Using debug hack from ropmount/ropfinding/intfilter/asmparser.py +311
        self.classes = asmparser.parse(None, self.archi)
        self.tests = []
        self.tests.append((self.classes['ConstMatcher'], self.consts))
        self.tests.append((self.classes['RegMatcher'], self.regs))
        self.tests.append((self.classes['MemAccessMatcher'], self.memaccess))
        self.tests.append((self.classes['InstrMatcher'], self.instrs))



class AsmParserTest(AsmTest):
    archi = None
    consts = {}
    regs = {}
    memaccess = {}
    instrs = {}

    def __init__(self):
        super(AsmParserTest, self).__init__()
        self.behaviour = {
                            "OK" : self.expect_ok,
                            "KO" : self.expect_ko,
                            "RAISE" : self.expect_raise
                         }

    def expect_ok(self, function, *args, **kwargs):
        assert_is_not_none(function(*args, **kwargs))

    def expect_ko(self, function, *args, **kwargs):
        assert_is_none(function(*args, **kwargs))

    def expect_raise(self, function, exc, *args, **kwargs):
        with assert_raises(exc):
            function(*args, **kwargs)

    @istest
    def do_test(self):
        for test_type, test_dict in self.tests:
            for expected, to_test in test_dict.items():
                if expected == "RAISE":
                    #dispatch exceptions
                    for exc, values_to_test in to_test.items():
                        for value in values_to_test:
                            yield self.behaviour["RAISE"], test_type.parse, exc, value
                else:
                    for value in to_test:
                        yield self.behaviour[expected], test_type.parse, value



@istest
class X86AsmParserTest(AsmParserTest):
    archi = archis["INTEL32"]

    consts = {
                "OK" : ["CONST", "0x10", "12"],
                "KO" : ["TRUC", "ANY"]
             }
    regs = {
                "KO" : ["TRUC", "ANY"],
                "OK" : ['REG32', 'REG16', 'REG8', 'REG', 'ESP', 'EAX', 'AX']
            }

    memaccess = {
                    "KO" : ['[]',
                      'ANY',
                      '[EAX + EDX]',
                      '[EAX + EDX + ECX]',
                      '[EAX + EDX + ECX + CONST]',
                      '[EAX + EDX * FAKE]',
                      '[EAX + BAD_REGISTER * 2]',
                      '[EAX + EDX * CONST + LOL]',
                      '[EAX + EDX * 3]',
                      '[REG32 + REG32 + CONST]'],

                  "OK" : [ '[ANY]',
                      '[EAX]',
                      '[EAX + EDX * 1]',
                      '[EAX + EDX * 2]',
                      '[EAX + EDX * CONST]',
                      '[EDX * 1]',
                      '[CONST]',
                      '[EAX + CONST]',
                      '[REG32 + REG32 * CONST + CONST]',
                      '[REG32]',
                      '[REG]']
                  }


    instrs = {
                "OK" : ['ret', 'mov REG32, REG32', 'mov REG32, [CONST]', 'ANY', 'retn CONST'],
                "RAISE" : {
                            ValueError : ["retn", "mov CONST, CONST"]
                          }
             }


class AsmMatcherTest(AsmTest):
    archi = None
    immed = {}
    regs = {}
    memaccess = {}

    def __init__(self):
        super(AsmMatcherTest, self).__init__()
        self.behaviour = {
                            "OK" : self.expect_ok,
                            "KO" : self.expect_ko,
                            "RAISE" : self.expect_raise
                         }
        if self.archi is not None:
            self.matcher_to_type = {
                    self.classes['ConstMatcher'] : lambda x : self.archi.Immediat(x, self.archi.Immediat.default_size),
                    self.classes['RegMatcher'] : self.archi.Register,
                    self.classes['MemAccessMatcher'] : lambda x: self.archi.MemAccess(*x),
                    }

    def expect_ok(self, function, *args, **kwargs):
        assert_is(True, function(*args, **kwargs))

    def expect_ko(self, function, *args, **kwargs):
        assert_is(False, function(*args, **kwargs))

    def expect_raise(self, function, exc, *args, **kwargs):
        with assert_raises(exc):
            function(*args, **kwargs)

    @istest
    def do_test(self):
        for test_type, test_dict in self.tests:
            for matcher_entry in test_dict:
                test_matcher = test_type.parse(matcher_entry)
                assert test_matcher
                for expected, to_test in test_dict[matcher_entry].items():
                    if expected == "RAISE":
                        #dispatch exceptions
                        for exc, values_to_test in to_test.items():
                            for value in values_to_test:
                                try:
                                    value = self.matcher_to_type[test_type](value)
                                    yield self.behaviour["RAISE"], test_matcher.match, exc, value
                                except exc:
                                    #Expected exception was raise during parsing
                                    assert_true(True)
                    else:
                        for value in to_test:
                            value = self.matcher_to_type[test_type](value)
                            yield self.behaviour[expected], test_matcher.match, value


class X86AsmMatcherTest(AsmMatcherTest):
    archi = archis["INTEL32"]
    consts = {
            'CONST' : { "OK": list(range(3)), "RAISE" :{ ValueError :  ["STR"]}},
                '0xa' : {"OK" : [10], "KO" : [9]},
                '2' : {"OK" : [2], "KO" : [1]}
            }

    regs = {
                'REG32' : {'OK' : ["EAX", "EDX", "ECX", "ESP"], "KO" : ["AX", "AH"]},
                'REG16' : {'OK' : ["AX", "DX", "CX", "SP"], "KO" : ["ESP", "EAX", "AH"]},
                'REG8' : {'OK' : ["AH", "AL", "CL", "DH"], "KO" : ["AX", "ESP", "EAX"]},
                'EAX' : {'OK' : ["EAX"], "KO" : ["AX", "AH", "EDX"]}
           }

    memaccess = {
            "[ANY]" : { "OK" : [(1,), ("EAX","EDX",2,0x1000), (None, None, 0, 0x1000)] },
            "[EDX]" : {
                    "OK" : [("EDX", None, 0, 0), ("EDX", None, 0, 0)],
                    "KO" : [("EDX", None, 0, 3), ("EAX", None, 0, 0), ("EDX", "EAX", 1 , 2)]  },
            "[EDX + 2]" : {
                    "OK" : [("EDX", None, 0, 2)],
                    "KO" : [("EDX", "EDX", 1, 3), ("EAX", None, 0, 2), ("EDX", "EAX", 1 , 2)]  },
            "[EAX * 4]" : {
                    "OK" : [(None, "EAX", 4, 0)],
                    "KO" : [("EAX", None, 0, 4), ("EAX", "EAX", 2, 0)]  },
            "[0x1000]" : {
                    "OK" : [(None, None, 0, 0x1000)],
                    "KO" : [("EAX", None, 0, 0x1000), (None, "EAX", 1, 0x1000), (None, None, 0, 0)]},
            "[EAX + REG32 * 2 + 0x5000]" : {
                "OK" : [("EAX", "EDX", 2, 0x5000), ("EAX", "EAX", 2, 0x5000) ],
                    "KO" : [("EAX", None, 0, 0x1000), ("EAX", "EBX", 1, 0x5000)]}
             }
