#!/usr/bin/python2

from nose.tools import *
from tests.mapfile_tests.fakemapfile import FakeMapFile
from ropmount.ropfinding.opcodefinder import OpcodeFinder

archi_name = "INTEL32"

def make_FMF():
    FMF = FakeMapFile(archi_name)
    FMF.define_mapping([
        (0x1000, "A" * 0x42 + "B\xc3B" + "C\xc3C" ),
        (0x10, "\xc3\xcc" + "A"  * 10 + "\xcc" + "A" + "\xc3"),
        (0x42, "A\xff\xfeAAA")])
    return FMF

@raises(StopIteration)
def Test_next_code_block():
    """ Test opcodefinder block iterator"""
    FMF = make_FMF()
    finder = OpcodeFinder(FMF)
    it = finder.code_block_iterator()
    mapp =  it.next()
    assert_equal(mapp.vaddr, 0x1000)
    mapp =  it.next()
    assert_equal(mapp.vaddr, 0x10)
    it.next()
    it.next()

def Test_next_instr():
    """ Test opcodefinder iter"""
    FMF = make_FMF()
    finder = OpcodeFinder(FMF)
    finder.rop_opcodes += ['\xcc', '\xc3']
    get = []
    for opcode, validator in finder:
        vaddr = validator.base_vaddr
        if vaddr in get:
            assert False
        get.append(vaddr)
        assert_equal(opcode , FMF.read_from_vaddr(validator.base_vaddr, len(opcode)))
    assert_equal(6, len(get))

