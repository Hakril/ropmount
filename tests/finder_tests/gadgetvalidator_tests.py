#!/usr/bin/python2

from ropmount.archi import archis
from ropmount.ropfinding.opcodefinder import GadgetValidator
from ropmount.mapfile.mapping import DummyMapping
from tests.utils import *
from nose.tools import *


class GadgetValidatorTest(ClassicTest):
    archi = None
    # (to_disas, start_offset) : [lists of expected instr]
    disas = {}


    def make_DB(self, block):
        return self.archi.DisasDB(DummyMapping(block))


    def dotest(self, validator, expected):
        for disas, expected_disas in zip(validator, expected):
            for instr, expected_instr in zip(disas, expected_disas):
                assert_equal(instr.mnemo, expected_instr)

    @istest
    def generate_test(self):
        for (opcodes, offset), expected in self.disas.items():
            yield self.dotest, GadgetValidator(self.make_DB(opcodes), offset), expected


class X86GadgetValidatorTest(GadgetValidatorTest):
    archi = archis["INTEL32"]
    # (to_disas, start_offset) : [list_of expected instr]
    disas = {
                #int3; nop; xor edx,edx
                ("\xcc\x90\x31\xd2", 2) : [("xor",) ,("nop", "xor"),("int3", "nop", "xor")],
                #int3; ret; xor edx,edx : must stop at the first ret we met
                ("\xcc\xc3\x31\xd2", 2) : [("xor",)],
                # xor; xor ; ret
                # gadget that begin with \xd2 consume ret => notvalid
                ("\x31\xd2\x31\xd2\xc3", 4) : [("ret",), ("xor", "ret"), ("xor", "xor", "ret")],
                # invalid; ret
                ("\x81\x81\xc3", 2) : [("ret",)],
                #Test from Starcraft and hidden push esp; ret
                # add esp, 0x54 ; ret
                # push esp; ret hidden (\x54\xc3)
                ("\x83\xc4\x54\xc3", 3) : [("ret",), ("push", "ret"), ("add", "ret")],
            }
