#!/usr/bin/python2

from ropmount.mapfile.mapping import DummyMapping
import ropmount.archi as archi

class FakeMapFile(object):
    """ A Map file based on simple string with X86 opcode for debug"""

    def __init__(self, archi_name, file=None):
        self.f = file
        self.__mapping = []
        self.archi_name = archi_name
        self.archi = archi.archis[archi_name]
        self.EIP = 0

    def define_mapping(self, mappings):
        """ define the mapping of FakeMapFile
        mapping arg = [(vaddr, "section")]"""
        self.__mapping = [DummyMapping(code, vaddr) for vaddr, code in mappings]

    # No file => No real offset
    def offset_from_vaddr(self, vaddr):
        return vaddr

    def get_mapping(self):
        return self.__mapping

    def read_from_vaddr(self, vaddr, size):
        for mapp in self.__mapping:
            if vaddr in mapp:
                 return mapp.get_code()[vaddr - mapp.vaddr: vaddr - mapp.vaddr + size]

