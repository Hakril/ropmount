from tests.mapfilefactory import *
from nose.plugins.skip import SkipTest, Skip
from  nose.tools import *
from tests.utils import *


class ExportTest(ClassicTest):
    code = ""
    targets = []
    expected_syms = []

    def applytest(self, target, expected_syms):
        facto = CMapFileFactory(target)
        if not facto.istestable():
            raise SkipTest("Untestable parameters : {0}".format(target))
        syms = facto.generate_mapfile([self.code]).get_symbols()
        for expected_symbol in expected_syms:
            assert_in(expected_symbol, syms)

    @istest
    def do_export_test(self):
        for target in self.targets:
            yield self.applytest, target, self.expected_syms

class SimpleExportTest(ExportTest):
    code = """
    int func()
    {
        return 1;
    }
    int main(int argc, char **argv)
    {
        return strlen(argv[0]) + func();
    }"""
    targets = [(ElfTarget, X86Target)]
    expected_syms = ["strlen", "func"]

#I haven't find a proper way to generate good PE with debug info on linux..
#mingw generate PE with DWARF only debug info..
#Use windows to compile/test ?
class PESimpleExportTest(SimpleExportTest):
    targets = [(PETarget, X86Target)]
    expected_syms = ["strlen"]
