#!/usr/bin/python2

from tests.mapfile_tests.fakemapfile import FakeMapFile
from nose.tools import *

archi_name = "INTEL32"

def test_simple_mapping():
    """  Fakemap test simple mapping """
    FMF = FakeMapFile(archi_name)
    FMF.define_mapping([(0x1000, "A" * 0x42 )])
    mapping = FMF.get_mapping()
    assert_equal(mapping[0].vaddr, 0x1000)
    assert_equal(mapping[0].size, 0x42)



def test_double_mapping():
    """  Fakemap test double mapping """
    FMF = FakeMapFile(archi_name)
    FMF.define_mapping([(0x1000, "A" * 0x42 ),(0x0, "A" * 0x100 )])
    mapping = FMF.get_mapping()
    assert_equal(mapping[0].vaddr, 0x1000)
    assert_equal(mapping[0].size, 0x42)
    assert_equal(mapping[1].vaddr, 0x0)
    assert_equal(mapping[1].size, 0x100)

def test_read_from_vaddr():
    """  Fakemap test read from vaddr """
    FMF = FakeMapFile(archi_name)
    s0 = "A" * 0x42 + "BBA"
    FMF.define_mapping([(0x1000, "A" * 0x42 + "BBA" ),(0x0,  "ABC" + "D" * 0x100 )])
    s = FMF.read_from_vaddr(1, 3)
    assert_equal(s, "BCD")
    s2 = FMF.read_from_vaddr(0x1041, 10)
    assert_equal(s2, "ABBA")
    s3 = FMF.read_from_vaddr(0x1000, 0x45 )
    assert_equal(s0, s3)

