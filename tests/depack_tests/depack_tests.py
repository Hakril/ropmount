#!/usr/bin/python2

from nose.tools import *
from StringIO import StringIO
import ropmount.utils.depack as depack


def test_simple_depack():
    """ Depack single value """
    descr = [("value", depack.BYTE)]
    s = StringIO("\x42")
    res = depack.depack(descr, s)
    assert_equal(res["value"], 0x42)

def test_multiple_depack():
    """ Depack multiple values """
    descr = [("value1", depack.BYTE), ("value2", depack.BYTE), ("value3", depack.BYTE)]
    s = StringIO("\x42\x00\x69")
    res = depack.depack(descr, s)
    assert_equal(res["value1"], 0x42)
    assert_equal(res["value2"], 0x00)
    assert_equal(res["value3"], 0x69)

def test_array_depack():
    """ Depack array """
    descr = [("array", depack.BYTE * 4)]
    s = StringIO("\x42\x00\x69\xff")
    res = depack.depack(descr, s)
    assert_equal(res["array"][0], 0x42)
    assert_equal(res["array"][1], 0x00)
    assert_equal(res["array"][2], 0x69)
    assert_equal(res["array"][3], 0xff)

def test_nested_depack():
    """ Depack nested struct """
    descr = [("value", depack.BYTE),
              ("struct", [ ("Svalue1", depack.BYTE), ("Svalue2", depack.BYTE)])
              ]
    s = StringIO("\x42\x00\x69")
    res = depack.depack(descr, s)
    assert_equal(res["value"], 0x42)
    # Is this really a recursive structure ?
    assert_equal(type(res["struct"]), type(res))
    assert_equal(res["struct"]["Svalue1"], 0x00)
    assert_equal(res["struct"]["Svalue2"], 0x69)


def test_struct_array():
    """ Depack array of struct """
    sub_descr = [("Svalue1", depack.BYTE), ("Svalue2", depack.BYTE)]
    descr = [("value", depack.BYTE),
             ("structs", (sub_descr,) * 2)]

    s = StringIO("\x42\x00\x69\x13\x37")
    res = depack.depack(descr, s)
    assert_equal(res["value"], 0x42)
    # Is this really a recursive structure ?
    assert_equal(type(res["structs"][0]), type(res))
    assert_equal(res["structs"][0]["Svalue1"], 0x00)
    assert_equal(res["structs"][0]["Svalue2"], 0x69)
    assert_equal(res["structs"][1]["Svalue1"], 0x13)
    assert_equal(res["structs"][1]["Svalue2"], 0x37)

def test_little_endian():
    """Depack test little endian """
    descr = [("value", depack.DWORD)]
    s = StringIO("\x42\x43\x44\x45")
    res = depack.depack(descr, s, depack.LITTLE_ENDIAN)
    assert_equal(res["value"], 0x45444342)

def test_big_endian():
    """Depack test big endian """
    descr = [("value", depack.DWORD)]
    s = StringIO("\x42\x43\x44\x45")
    res = depack.depack(descr, s, depack.BIG_ENDIAN)
    assert_equal(res["value"], 0x42434445)

