


class Dynamicable(type):
    """A metaclass that allow to set cls.__test__ dynamicaly by calling cls.is_testable"""

    __test__ = False

    def __getattribute__(cls, value):
        if value == "__test__":
            return cls.istestable()
        return super(Dynamicable, cls).__getattribute__(value)



class TestIfConfig(object):
    """ A class with test that can be triggered using istestable() """
    __metaclass__ = Dynamicable

    @staticmethod
    def istestable():
        return True

class ClassicTest(TestIfConfig):
    """ the base class of all RopMount test """
