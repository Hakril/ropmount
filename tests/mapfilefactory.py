#!/usr/bin/python2

import subprocess
import platform
import ropmount.mapfile as MapFile
from nose.plugins.skip import SkipTest, Skip
from ropmount.ropasm.assembler import RopAssembler



class TargetParameter(object):
    @classmethod
    def get_flag(cls):
        raise NotImplementedError("{0} doesn't define the function".format(cls.__name__))

    @classmethod
    def istestable(cls):
        raise NotImplementedError("{0} doesn't define the function".format(cls.__name__))

class CompilerTarget(TargetParameter):
    compiler = None

    @classmethod
    def get_flag(cls):
        return cls.compiler

    @classmethod
    def istestable(cls):
        try:
            p = subprocess.Popen(cls.compiler, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.communicate()
        #Found but compiler without option fail
        except OSError as e:
            #No such file or Directory
            if e.errno == 2:
                raise SkipTest("Untestable Compiler <{0}>".format(cls.compiler))
            raise
        return True

class ElfTarget(CompilerTarget):
    compiler = "gcc"

class PETarget(CompilerTarget):
    compiler = "i686-w64-mingw32-gcc"

class RelocatableTarget(TargetParameter):
    @classmethod
    def get_flag(cls):
        return ["-fPIC", "--shared"]

    @classmethod
    def istestable(cls):
        return True

class X86Target(TargetParameter):
    @classmethod
    def get_flag(cls):
        return ["-m32"]

    @classmethod
    def istestable(cls):
        return platform.machine() in ["x86_64", "i386"]

class X86_64Target(TargetParameter):
    @classmethod
    def get_flag(cls):
        return ["-m64"]

    @classmethod
    def istestable(cls):
        return platform.machine()  == "x86_64"

class MapFileFactory(object):
    file_name = "/tmp/gad.S"
    output_file = "/tmp/out"
    default_args = ["-nostdlib", "-o", output_file, file_name]

    header = [".intel_syntax\n",
              ".global _start\n",
              "_start :\n"]

    def __init__(self, parameters):
        self.compiler = parameters[0]
        if not issubclass(self.compiler, CompilerTarget):
            raise ValueError("{0} first argument must be CompilerTarget".format(self.__class__.__name__))
        self.parameters = parameters[1:]

    def istestable(self):
        return self.compiler.istestable() and all([param.istestable() for param in self.parameters])

    def generate_mapfile(self, gadgets):
        f_asm = open(self.file_name, 'w+')
        f_asm.writelines(self.header)
        f_asm.write("\n".join(gadgets))
        f_asm.flush()
        f_bin = open(self.output_file, 'w')
        args = list(self.default_args)
        for param in self.parameters:
            args = param.get_flag() + args
        command = [self.compiler.get_flag()] + args
        print "Call : <{0}>".format(" ".join(command))
        print ("====== FILE CONTENT =====")
        f_asm.seek(0)
        print(f_asm.read())
        print ("====== END CONTENT =====")
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            print ("====== ERROR =====")
            print (stderr)
            print ("====== END ERROR =====")
            raise subprocess.CalledProcessError(p.returncode, " ".join(command), output=stderr)
        m = MapFile.create_mapfile(f_bin.name)
        f_asm.close()
        f_bin.close()
        return m

    def __repr__(self):
        return "<{0}[{1}]>".format(self.__class__.__name__, ",".join([str(param.__name__) for param in self.parameters]))

class CMapFileFactory(MapFileFactory):
    file_name = "/tmp/code.c"
    output_file = "/tmp/out"
    default_args = ["-o", output_file, file_name]
    header = """#include <stdlib.h>
    #include <stdio.h>"""


def generate_assembler(target, gadgets):
    x = MapFileFactory(target)
    mapf = x.generate_mapfile(gadgets)
    return RopAssembler(mapf)
