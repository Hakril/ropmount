from ropmount.mapfile.mapfile import RelocableDumpError
from tests.utils import *
from tests.mapfilefactory import *
from nose.plugins.skip import SkipTest, Skip
from  nose.tools import *

class SimpleScenario(ClassicTest):
    targets = {
            (ElfTarget, X86_64Target) : ["ret"],
            (ElfTarget, X86Target) : ["ret"]
            }
    parameters = ()


    def applytest(self, mapfilefactory, code):
        mapfile = mapfilefactory.generate_mapfile([code])
        start = mapfile.get_symbols()["_start"]
        assert_equals(start.dump().value, start.value)

    @istest
    def generate_test(self):
        for target in self.targets:
            mapfilefactory = MapFileFactory(target + self.parameters)
            if not mapfilefactory.istestable():
                raise SkipTest()
            for code in self.targets[target]:
                yield self.applytest, mapfilefactory, code


class RelocableFixBaseScenario(SimpleScenario):
    targets = {
            (ElfTarget, X86_64Target) : ["ret"],
            (ElfTarget, X86Target) : ["ret"]
            }
    parameters = (RelocatableTarget,)


    def applytest(self, mapfilefactory, code):
        mapfile = mapfilefactory.generate_mapfile([code])
        start = mapfile.get_symbols()["_start"]
        with assert_raises(RelocableDumpError):
            start.dump()
        base = 0xc00000
        mapfile.fix_baseaddr(base)
        dumped = start.dump()
        assert_equals(dumped.value, start.value + base)

class RelocableFixSymbolScenario(SimpleScenario):
    targets = {
            (ElfTarget, X86_64Target) : ["ret"],
            (ElfTarget, X86Target) : ["ret"]
            }
    parameters = (RelocatableTarget,)


    def applytest(self, mapfilefactory, code):
        mapfile = mapfilefactory.generate_mapfile([code])
        start = mapfile.get_symbols()["_start"]
        _start_addr = 0xc4239
        mapfile.fix_base_from_symbol('_start', _start_addr)
        assert_equals(start.dump().value, _start_addr)
        #Use dump start.value to check base_addr expected value
        assert_equals(mapfile._base_addr, _start_addr - start.value)
