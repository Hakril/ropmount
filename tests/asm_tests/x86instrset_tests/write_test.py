#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def writeintreg_simple_test():
    """Test write int to reg with retn"""
    gadgets = ["pop %edx;ret"]
    gadgets += ["mov [%ecx], %edx;ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242")
    assert x

def writeintreg_simple2_test():
    """Test write int to reg with retn and garbage ROP instrs"""
    gadgets = ["pop %edx;ret"]
    gadgets += ["mov [%ecx], %edx;pop %edx; inc %eax; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242")
    assert x

def writeintreg_complexe_test():
    """Test complex (mov) write int to reg with retn"""
    gadgets = ["pop %edx;ret"]
    gadgets += ["mov %eax,%ecx;ret"]
    gadgets += ["mov [%eax], %edx;ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242")
    assert x

def writeintreg_simple_noreg_test():
    """Test no_regs with simple int to reg write"""
    gadgets = ["pop %edx;ret"]
    gadgets += ["mov [%ecx], %edx;pop %eax;ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242!eax")
    assert not x

def writeintreg_complexe_noreg_test():
    """Test no_regs with complex int to reg write """
    gadgets = ["pop %edx; pop %ebp; ret"]
    gadgets += ["mov %eax,%ecx; pop %esi; ret"]
    gadgets += ["mov [%eax], %edx; pop %edi; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242!edi")
    assert not x
    x = ass.assemble("write ecx, 0x42424242!esi")
    assert not x
    x = ass.assemble("write ecx, 0x42424242!ebp")
    assert not x

def writeintreg_src_noreg_test():
    """Test no_regs with src in no_reg"""
    gadgets = ["pop %edx; pop %ebp; ret"]
    gadgets += ["mov %eax,%ecx; pop %esi; ret"]
    gadgets += ["mov [%eax], %edx; pop %edi; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242!edx")
    assert not x

def writeintreg_preserv_dst_test():
    """Test write int to reg preserv dest"""
    gadgets = ["pop %edx; pop %ebp; ret"]
    gadgets += ["mov %eax,%ecx; pop %esi; ret"]
    gadgets += ["mov [%eax], %edx; pop %ecx; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, 0x42424242")
    print x
    assert not x

#Reg to Reg

def write_regreg_simple_test():
    """Test write reg to reg with retn"""
    gadgets = ["mov [%ecx], %edx;ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edx")
    assert x

def write_regreg_simple2_test():
    """Test write reg to reg with retn and garbage ROP instrs"""
    gadgets = ["mov [%ecx], %edx;pop %ebx; inc %eax; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edx")
    assert x
    x = ass.assemble("write ecx, edx!ebx")
    assert not x

def write_regreg_simple_noreg_test():
    """Test no_regs with write reg to reg"""
    gadgets = ["mov [%ecx], %edx;pop %edx; inc %eax; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    #must fail because write don't change src
    x = ass.assemble("write ecx, edx")
    assert not x

def write_regreg_simple_noreg2_test():
    """Test no_regs with write reg to reg"""
    gadgets = ["mov [%ecx], %edx;inc %eax; ret 0x2"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edx")
    assert x
    x = ass.assemble("write ecx, edx!eax")
    assert not x

def write_regreg_complex_noreg_test():
    """Test no_regs with complexe write reg to reg"""
    gadgets = ["mov [%ecx], %edx;pop %edx; inc %eax; ret 0x2"]
    gadgets += ["mov %ecx, %esi ; ret"]
    gadgets += ["mov %edx, %edi ; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write esi, edi")
    assert x
    x = ass.assemble("write esi, edi!eax")
    assert not x

def write_regreg_complex_noreg2_test():
    """Test no_regs with complexe write reg to reg 2 """
    gadgets = ["mov [%ecx], %edx;pop %edx; inc %eax; ret 0x2"]
    gadgets += ["mov %edx, %edi ; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edi")
    assert x
    x = ass.assemble("write ecx, edi!eax")
    assert not x

def write_regreg_complex_noreg3_test():
    """Test no_regs with complexe write reg to reg 3 """
    gadgets = ["mov [%ecx], %edx;pop %edx; inc %eax; ret 0x2"]
    gadgets += ["mov %edx, %edi ; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edi")
    assert x
    x = ass.assemble("write ecx, edi!eax")
    assert not x

def write_regreg_preserv_src_test():
    """Test complexe write preserv src reg"""
    gadgets = ["mov [%ecx], %edx;pop %edi; inc %eax; ret 0x2"]
    gadgets += ["mov %edx, %edi ; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write ecx, edi")
    assert not x

def write_regreg_preserv_dst_test():
    """Test complexe write preserv dst reg"""
    gadgets = ["mov [%ecx], %edx;pop %edi; inc %eax; ret 0x2"]
    gadgets += ["mov %ecx, %edi ; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("write edi, edx")
    assert not x
