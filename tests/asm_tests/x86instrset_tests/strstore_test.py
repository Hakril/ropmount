#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def strstore_toint_simple_test():
    """Test strstore to int with retn"""
    #write
    gadgets = ["mov [%edx], %ecx; ret 0x4"]
    gadgets += ["pop %ecx; ret"]
    gadgets += ["pop %edx; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('strstore 0x1000, testing')
    assert x

def strstore_toreg_simple_test():
    """Test strstore to reg with retn"""
    #write
    gadgets = ["mov [%edx], %ecx; ret 0x4"]
    gadgets += ["pop %ecx; ret"]
    gadgets += ["pop %edx; ret"]
    gadgets += ["inc %edx; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('strstore edx, testing')
    assert x

def strstore_toreg_complexe_test():
    """Test strstore to reg with retn and mov"""
    #write
    gadgets = ["mov [%edx], %ecx; ret 0x4"]
    gadgets += ["pop %ecx; ret"]
    gadgets += ["pop %edx; ret"]
    gadgets += ["inc %edi; ret"]
    gadgets += ["mov %edx, %edi; ret 0x4"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('strstore edi, testing')
    assert x
