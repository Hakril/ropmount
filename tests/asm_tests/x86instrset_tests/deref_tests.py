#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *


def deref_simple_test():
    """Test simple deref"""
    gadgets = ["mov %edx, [%edx];pop %edi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("deref edx")
    assert x
    assert_equal(len(x.stack.values), 1)

def deref_simple_noreg_test():
    """Test no_regs on simple deref"""
    gadgets = ["mov %edx, [%edx];pop %edi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("deref edx!edi")
    assert not x

def deref_complex_test():
    """Test complex deref"""
    gadgets = ["mov %edx, [%edx];pop %edi;ret"]
    gadgets =  ["mov %edx, %eax;pop %esi;ret"]
    gadgets += ["mov %ecx, [%edx];pop %esi;ret"]
    gadgets += ["mov %eax, %ecx;pop %esi;ret"]
    gadgets += ["mov %edx, %ecx;pop %esi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("deref edx")
    assert x
    x = ass.assemble("deref eax")
    assert x
    x = ass.assemble("deref ecx")
    assert x

def deref_simple_noreg_test():
    """Test no_regs on complex deref"""
    gadgets =  ["mov %edx, %eax;pop %esi;ret"]
    gadgets += ["mov %ecx, [%edx];pop %esi;ret"]
    gadgets += ["mov %eax, %ecx;pop %esi;ret"]
    gadgets += ["mov %edx, %ecx;pop %esi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("deref edx!esi")
    assert not x
    x = ass.assemble("deref eax!ecx")
    assert not x
