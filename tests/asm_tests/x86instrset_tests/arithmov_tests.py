#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def arithmov_simple_test():
    """Test simple arithmov"""
    gadgets = ["xor %eax, %ebx;ret"]
    gadgets += ["add %edx, %ecx;ret"]
    gadgets += ["pop %eax;ret"]
    gadgets += ["pop %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("armov eax, ebx")
    assert x
    x = ass.assemble("armov edx, ecx")
    assert x

def arithmov_garbage_test():
    """Test simple arithmov with gargabe and retn"""
    gadgets = ["xor %eax, %ebx; inc %esi ;ret 0x4"]
    gadgets += ["add %edx, %ecx; inc %edi ;ret 0x4"]
    gadgets += ["pop %eax;ret"]
    gadgets += ["pop %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("armov eax, ebx")
    assert x
    x = ass.assemble("armov edx, ecx")
    assert x

def arithmov_noregs_test():
    """Test no_regs on arithmov"""
    gadgets = ["xor %eax, %ebx; inc %esi ;ret 0x4"]
    gadgets += ["add %edx, %ecx; inc %edi ;ret 0x4"]
    gadgets += ["pop %eax;ret"]
    gadgets += ["pop %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("armov eax, ebx!esi")
    assert not x
    x = ass.assemble("armov edx, ecx!edi")
    assert not x
