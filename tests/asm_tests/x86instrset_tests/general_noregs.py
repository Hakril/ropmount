#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def noreg_mul_test():
    """Test that 'mul' invalid EAX/EDX"""
    gadgets =  ["pop %ecx; mul %ecx; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("set ecx,0x42")
    assert x
    x = ass.assemble("set ecx,0x42!edx")
    assert not x
    x = ass.assemble("set ecx,0x42!eax")
    assert not x

def noreg_div_test():
    """Test that 'div' invalid EAX/EDX"""
    gadgets =  ["pop %ecx; div %ecx; ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("set ecx,0x42")
    assert x
    x = ass.assemble("set ecx,0x42!edx")
    assert not x
    x = ass.assemble("set ecx,0x42!eax")
    assert not x
