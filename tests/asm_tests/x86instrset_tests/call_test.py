#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def call_test():
    """Test call"""
    gadgets =  ["add %esp, 0x8; pop %ecx;ret"]
    #declaring a function
    gadgets +=  [".global func"]
    gadgets +=  ["func:"]
    gadgets +=  ["nop"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("call func,0x42,0x69")
    assert x
    x = ass.assemble("call func,0x42,0x69,0x32,0x77")
    assert not x

def call_immed_test():
    """Test call an Immediat"""
    gadgets =  ["add %esp, 0x8; pop %ecx;ret"]
    #declaring a function
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("call 0xDEADBEEF,0x42,0x69")
    assert x
    x = ass.assemble("call 0xDEEADBEEF,0x42,0x69,0x32,0x77")
    assert not x
