#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *


def simple_cpy_test():
    """Test simple cpy"""
    gadgets = ["mov %edx, %ecx;ret"]
    gadgets += ["mov %edi, %edx;pop %edx;ret"]
    gadgets += ["mov %esi, %edi;pop %edi;ret"]
    gadgets += ["mov %edi, %esi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("cpy edx, ecx")
    assert x

def return_cpy_test():
    """Test cpy with mov in two sens"""
    gadgets = ["mov %edi, %edx;pop %edx;ret"]
    gadgets += ["mov %esi, %edi;pop %edi;ret"]
    gadgets += ["mov %edi, %esi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble("cpy edx, edi")
    assert not x
    x = ass.assemble("cpy edi, esi")
    assert x
