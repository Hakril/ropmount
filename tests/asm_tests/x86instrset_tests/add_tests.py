#!/usr/bin/python2

from nose.tools import *
from tests.mapfilefactory import *

def add_simple_test():
    """Test simple add"""
    gadgets =  ["add %ecx, 4;ret"]
    gadgets +=  ["inc %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('add ecx, 4')
    assert x
    #Not implemented Yet..
    #TODO
    #x = ass.assemble('add ecx, 8')
    #assert x
    x = ass.assemble('add edx, 4')
    assert x
    #Don't use inc for big add
    x = ass.assemble('add edx, 10')
    assert not x

def complex_add_test():
    """Test complex add"""
    gadgets =  ["add %ecx, %edx;ret"]
    gadgets +=  ["pop %edx;ret"]
    gadgets +=  ["add %edi, %edx;ret"]
    gadgets +=  ["pop %edi;ret"]
    gadgets +=  ["mov %edx, %esi;ret"]
    gadgets +=  ["mov %esi, %edi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('add ecx, 4')
    assert x
    x = ass.assemble('add esi, 0x42')
    assert x

def addinc_simple_test():
    """Test simple add inc"""
    gadgets =  ["mov %ecx, %edx;ret"]
    gadgets +=  ["inc %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('add ecx, 4')
    assert not x
    x = ass.assemble('add edx, 4')
    assert x

def addinc_complex_test():
    """Test complex add inc"""
    gadgets =  ["mov %ecx, %edx;ret"]
    gadgets +=  ["mov %edx, %ecx;ret"]
    gadgets +=  ["inc %edx;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('add ecx, 4')
    assert x

def add_noreg_test():
    """Test add no_regs"""
    gadgets =  ["add %edi, %edx;ret"]
    gadgets +=  ["pop %edi;ret"]
    gadgets +=  ["mov %edx, %esi;ret"]
    gadgets +=  ["mov %esi, %edi;ret"]
    ass = generate_assembler([ElfTarget, X86Target], gadgets)
    x = ass.assemble('add ecx, 4!edi')
    assert not x
